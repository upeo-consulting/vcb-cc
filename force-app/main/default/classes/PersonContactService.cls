public with sharing class PersonContactService {
    
    public PersonContactService() {
        
    }
    
    public void setPersonContactCheckboxes (map<Id, SObject> newmap){
        
        List<Contact> personContactList = new List<Contact>();
        
        for (SObject recAccount : newmap.values()) {
            boolean syncMC = (Boolean) recAccount.get('Sync_to_MC__c');
            boolean allowMC = (Boolean) recAccount.get('Allow_in_MC__c');
            String PCId = (String) recAccount.get('PersonContactId');
            Contact personContact = new Contact();
            personContact.Id = PCId;
            personContact.Sync_to_MC__c = TRUE;
            personContact.Allow_in_MC__c = TRUE;
            personContactList.add(personContact);    
            system.debug(personContact);
            
        }
        
        update personContactList;
        
        
    }
    
}