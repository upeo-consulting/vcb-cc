/**
 * This Class that drives the ticking of the checkboxes used to indicate whether a record should be synced to Marketing Cloud.
 * The class has been made object generic. The class depends on MarketingCloudSyncBoxRelations__mdt in which we store per SObject type which
 * fields and query statements are used for the query in MCSyncSelector. The MCSyncSelector returns the list of records to be updated.
 */
@IsTest public with sharing class MarketingCloudSyncServiceTest {
    // Quote Object PositiveTestCases
   @IsTest static void quotesObjectPositiveTest() {
      Id RecordType = GlobalUtils.getRecordTypeIdByObjectAndDeveloperName('Account', 'PersonAccount');
      Account account = new Account();
      account.LastName='TestPersonAccount';
      account.PersonEmail='Test@gmail.com';
      account.RecordTypeId = RecordType;
      Insert account; 
      
      Opportunity oppObj = new Opportunity();
      oppObj.Name='101';
      oppObj.accountId=account.id;
      oppObj.Closedate=date.today().addDays(+10);
      oppObj.StageName='Open';
      Insert oppObj; 
      
      RecursiveTriggerHandler.isFirstTime = true;
      
      Quote quote=new Quote();
      quote.OpportunityId=oppObj.id;
      quote.Name='104';
      quote.Sync_to_MC__c=True;
      Insert quote;
      List<Quote> quoteRecord= [SELECT Id, Sync_to_MC__c FROM Quote where id=:quote.Id and Opportunity.Account.Sync_to_MC__c = TRUE];
      System.assertEquals(True,quoteRecord.size()>0);
      System.assertEquals(True,quoteRecord[0].Sync_to_MC__c);
    }  
    // Update Quote Object PositiveTestCases
    @IsTest static void quotesObjectPositiveTestUpdate() {
      Id RecordType = GlobalUtils.getRecordTypeIdByObjectAndDeveloperName('Account', 'PersonAccount');
      Account account = new Account();
      account.LastName='TestPersonAccount';
      account.PersonEmail='Test@gmail.com';
      account.RecordTypeId = RecordType;
      Insert account; 
      
      Opportunity oppObj = new Opportunity();
      oppObj.Name='101';
      oppObj.accountId=account.id;
      oppObj.Closedate=date.today().addDays(+10);
      oppObj.StageName='Open';
      Insert oppObj; 
      
      Quote quote=new Quote();
      quote.OpportunityId=oppObj.id;
      quote.Name='104';
      Insert quote;
      
      RecursiveTriggerHandler.isFirstTime = true;
      quote.Sync_to_MC__c=True;
      Update quote;
      
      List<Quote> quoteRecord= [SELECT Id, Sync_to_MC__c FROM Quote where id=:quote.Id and Opportunity.Account.Sync_to_MC__c = TRUE];
      System.assertEquals(True,quoteRecord.size()>0);
      System.assertEquals(True,quoteRecord[0].Sync_to_MC__c);
    }
    
    // Quote Objects Negative Test Case
    @IsTest static void quotesObjectNegativeTest() {
      Id personAccount =Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
      Account account = new Account();
      account.RecordtypeId=personAccount;
      account.Lastname='Test Account';
      account.Email__c='Test@gmail.com';
      Insert account; 
    
      Opportunity oppObj = new Opportunity();
      oppObj.Name='101';
      oppObj.accountId=account.id;
      oppObj.Closedate=date.today().addDays(+10);
      oppObj.StageName='Open';
      Insert oppObj;

      RecursiveTriggerHandler.isFirstTime = true;

      Quote quote=new Quote();
      quote.OpportunityId=oppObj.id;
      quote.Name='104';
      Insert quote;

      List<Quote> quoteObjRecord= [SELECT Id, Sync_to_MC__c,Opportunity.Account.Sync_to_MC__c FROM Quote where id=:quote.Id and Opportunity.Account.Sync_to_MC__c = True];
      System.assertEquals(False,quoteObjRecord.size()>0);
      }
      
   //AccountRelationship Object Positive Test Cases
    @IsTest static void  AccountRelationshipsObjectPositiveTest() {
       Account accountFrom = new Account();
       accountFrom.name='TestFrom Account';
       accountFrom.Email__c='TestFrom@gmail.com';
       accountFrom.Sync_to_MC__c=True;
       insert accountFrom;
       
       Account accountTo = new Account();
       accountTo.name='Test To Account';
       accountTo.Email__c='TestTo@gmail.com';
       accountTo.Sync_to_MC__c=True;
       Insert accountTo;
       
       RecursiveTriggerHandler.isFirstTime = true;
       
       Account_Relationship__c accountRelationshipObj=new Account_Relationship__c();
       accountRelationshipObj.Account_From__c=accountFrom.Id;
       accountRelationshipObj.Account_To__c=accountTo.Id;
       accountRelationshipObj.Role__c='Employer';
       accountRelationshipObj.Sync_to_MC__c=True;
       Insert accountRelationshipObj;
      
      List<Account_Relationship__c> accountRelationship =[SELECT Id, Sync_to_MC__c FROM Account_Relationship__c where Id=:accountRelationshipObj.Id and Account_From__r.Sync_to_MC__c = TRUE AND Account_To__r.Sync_to_MC__c = TRUE];
       System.assertEquals(True,accountRelationship.size()>0);
       System.assertEquals(True,accountRelationship[0].Sync_to_MC__c);
      }
   //Update AccountRelationship Object Positive Test Cases
    @IsTest static void  AccountRelationshipsObjectPositiveTestUpdate() {
       Account accountFrom = new Account();
       accountFrom.name='TestFrom Account';
       accountFrom.Email__c='TestFrom@gmail.com';
       accountFrom.Sync_to_MC__c=True;
       insert accountFrom;
       
       Account accountTo = new Account();
       accountTo.name='Test To Account';
       accountTo.Email__c='TestTo@gmail.com';
       accountTo.Sync_to_MC__c=True;
       Insert accountTo;
       Account_Relationship__c accountRelationshipObj=new Account_Relationship__c();
       accountRelationshipObj.Account_From__c=accountFrom.Id;
       accountRelationshipObj.Account_To__c=accountTo.Id;
       accountRelationshipObj.Role__c='Employer';
       accountRelationshipObj.Sync_to_MC__c=True;
       Insert accountRelationshipObj;
       
       RecursiveTriggerHandler.isFirstTime = true;
       accountRelationshipObj.Sync_to_MC__c=True;
       Update accountRelationshipObj;
       
      List<Account_Relationship__c> accountRelationship =[SELECT Id, Sync_to_MC__c FROM Account_Relationship__c where Id=:accountRelationshipObj.Id and Account_From__r.Sync_to_MC__c = TRUE AND Account_To__r.Sync_to_MC__c = TRUE];
       System.assertEquals(True,accountRelationship.size()>0);
       System.assertEquals(True,accountRelationship[0].Sync_to_MC__c);
      }
       
     //AccountRelationship Object Negative Test Cases 
     @IsTest static void  AccountRelationshipsObjectNegativeTest() {
      Id personAccount =Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

       Account accountFrom = new Account();
       accountFrom.RecordtypeId=personAccount;
       accountFrom.Lastname='TestFrom Account';
       accountFrom.Email__c='TestFrom@gmail.com';
       insert accountFrom;
       
       Account accountTo = new Account();
       accountTo.RecordtypeId=personAccount;
       accountTo.Lastname='Test To Account';
       accountTo.Email__c='TestTo@gmail.com';
       Insert accountTo;
       
       RecursiveTriggerHandler.isFirstTime = true;
       
       Account_Relationship__c accountRelationshipObj=new Account_Relationship__c();
       accountRelationshipObj.Account_From__c=accountFrom.Id;
       accountRelationshipObj.Account_To__c=accountTo.Id;
       accountRelationshipObj.Role__c='Employer';
       accountRelationshipObj.Sync_to_MC__c=True;
       Insert accountRelationshipObj;
      
       List<Account_Relationship__c> accountRelationshipRecord =[SELECT Id, Sync_to_MC__c FROM Account_Relationship__c where Id=:accountRelationshipObj.Id and Account_From__r.Sync_to_MC__c = True AND Account_To__r.Sync_to_MC__c = TRUE];
        System.assertEquals(False,accountRelationshipRecord.size()>0);
     }
     //AccountContactRelations Object Positive Test Cases   
    @IsTest static void AccountContactRelationsObjectPositiveTest() {
      Account account = new Account();
      account.name='Account_Test';
      account.Email__c='pos@gmail.com';
      Insert account;
      
      Account accountObj = new Account();
      accountObj.name='Positive case Account';
      accountObj.Sync_to_MC__c = TRUE;
      accountObj.Email__c='pos@gmail.com';
      Insert accountObj;
      
      Contact contact =new Contact();
      contact.FirstName='Test';
      contact.LastName='Contact';
      contact.Sync_to_MC__c = TRUE;
      contact.AccountId=accountObj.Id;
      Insert contact; 
      
      RecursiveTriggerHandler.isFirstTime = true;
      
      AccountContactRelation accountContRelObj =new AccountContactRelation();
      accountContRelObj.AccountId=account.Id;
      accountContRelObj.ContactId=contact.Id;
      accountContRelObj.Sync_to_MC__c=True;
      Insert accountContRelObj;
      
      List<AccountContactRelation> accountContactRelations= [SELECT Id, Sync_to_MC__c FROM AccountContactRelation where id=:accountContRelObj.Id and Contact.Sync_to_MC__c = TRUE];
      System.assertEquals(True,accountContactRelations.size()>0);
      System.assertEquals(True,accountContactRelations[0].Sync_to_MC__c);
    } 
      
   // AccountContactRelations Object Negative Test Case    
   @IsTest static void AccountContactRelationsObjectNegativeTest() {
      Id personAccount =Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
      Account account = new Account();
      account.RecordtypeId=personAccount;
      account.Lastname='TestACR Account';
      account.Email__c='negacr@gmail.com';
      Insert account;
      
      Account accountObj = new Account();
      accountObj.name='TestACR Account';
      accountObj.Email__c='negacr@gmail.com';
      Insert accountObj;
      
      Contact contact =new Contact();
      contact.FirstName='TFirstName';
      contact.LastName='TLastName';
      contact.AccountId=accountObj.Id;
      contact.Sync_to_MC__c=False;
      Insert contact; 
      
      RecursiveTriggerHandler.isFirstTime = true;
      
      AccountContactRelation accountContRelObj =new AccountContactRelation();
      accountContRelObj.AccountId=account.Id;
      accountContRelObj.ContactId=contact.Id;
      Insert accountContRelObj;
     
      List<AccountContactRelation> accountContact= [SELECT Id, Sync_to_MC__c FROM AccountContactRelation where id=:accountContRelObj.Id and Contact.Sync_to_MC__c = True];
      System.assertEquals(False,accountContact.size()>0);
      }
   
 }