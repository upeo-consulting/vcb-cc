/**
 * Created by Frederik on 7/05/2021.
 */

public interface IContactSelector {
    List<Contact> getContactByEmail(Map<Id, String> emailAddresses, String queryFields, List<String> accountIds);
}