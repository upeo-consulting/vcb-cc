/**
 * Created by Frederik Pardon on 07/04/2021.
 */

public with sharing class QuoteTriggerHandler extends TriggerHandler {


    protected override void onAfterInsert(Map<Id, SObject> newMap) {
        IMCSyncSelector mCSyncSelector = (IMCSyncSelector) new MCSyncSelector();
        MarketingCloudSyncService marketingCloudSyncService = (MarketingCloudSyncService) new MarketingCloudSyncService(mCSyncSelector);
        IContactSelector contactSelector = (ContactSelector) new ContactSelector();
        IQuoteSelector quoteSelector = (QuoteSelector) new QuoteSelector();
        IDuplicateSelector duplicateSelector = (DuplicateSelector) new DuplicateSelector();
        QuoteService quoteService = (QuoteService) new QuoteService(contactSelector, quoteSelector);
        DuplicateService duplicateService = (DuplicateService) new DuplicateService(duplicateSelector);


        if(RecursiveTriggerHandler.isFirstTime == true) {
            RecursiveTriggerHandler.isFirstTime = false;
            marketingCloudSyncService.setCheckBoxes((map<Id, SObject>) newMap);
            quoteService.setQuoteContact((Map<Id, Quote>) newMap);
            duplicateService.checkDuplicateRecords((map<Id, SObject>) newMap);
        }




    }

    protected override void onAfterUpdate(Map<Id, SObject> newMap, Map<Id, SObject> oldMap) {
        IMCSyncSelector mCSyncSelector = (IMCSyncSelector) new MCSyncSelector();
        MarketingCloudSyncService marketingCloudSyncService = (MarketingCloudSyncService) new MarketingCloudSyncService(mCSyncSelector);
        IContactSelector contactSelector = (ContactSelector) new ContactSelector();
        IQuoteSelector quoteSelector = (QuoteSelector) new QuoteSelector();
        QuoteService quoteService = (QuoteService) new QuoteService(contactSelector, quoteSelector);

        if(RecursiveTriggerHandler.isFirstTime == true) {
            RecursiveTriggerHandler.isFirstTime = false;
            marketingCloudSyncService.setCheckBoxes((map<Id, SObject>) newMap);
            quoteService.setQuoteContact((Map<Id, Quote>) newMap);
            quoteService.setSelektCarFields((Map<Id, Quote>) newMap);

        }
    }

}