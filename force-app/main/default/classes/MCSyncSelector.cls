/**
 * Created by Frederik on 20/04/2021.
 */

public with sharing class MCSyncSelector implements IMCSyncSelector  {

    public static List<Sobject> getRecordsById(List<String> Ids, String objectType, String fieldNames, String fieldToUpdate) {

        string selectString = 'SELECT Id, ' +fieldToUpdate;
        string fromString = ' FROM ' +objectType;
        string whereString = ' WHERE ' +fieldNames + ' AND Id in :Ids';

        system.debug(selectString);
        system.debug(fromString);
        system.debug(whereString);
        system.debug('queryString = ' +selectString +fromString +whereString);


        return database.query(selectString + fromString + whereString);
    }
}