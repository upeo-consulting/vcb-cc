@IsTest
public with sharing class IndividualTriggerHandlerTest {

 
    @IsTest static void testIndividual() {
    
        Individual ind = new Individual(LastName = 'Test');
        upsert ind;
        
        system.assertEquals('Test', ind.LastName);
        
    }
}