/**
 * @description       : Test class for the trigger handler
 * @author            : 4C Consulting NV
 * @group             : Trigger Handler
 * @last modified on  : 10-08-2020
 * @last modified by  : 4C Consulting NV
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   06-05-2020   Maarten.Devos@weare4c.com      Initial Version
**/

@IsTest
public with sharing class TriggerHandlerTest {   
    
	/**
    * @description Test the trigger activation mechanism
    * @return void 
    **/
    @isTest
    public static void TriggersActivated(){
        Trigger_Activation__c triggerActivation = new Trigger_Activation__c(
            SetupOwnerId = UserInfo.getUserId(),
            Disable_All_Apex_Triggers__c = true            
        );
        insert triggerActivation;
        
        Test.startTest();
        
        Boolean triggersActivated = TriggerHandler.triggersActivated();
        System.assertEquals(false, triggersActivated, 'Triggers have not been disabled on user level');
        
        triggerActivation.SetupOwnerId = UserInfo.getProfileId();
        update triggerActivation;        
        triggersActivated = TriggerHandler.triggersActivated();
        System.assertEquals(false, triggersActivated, 'Triggers have not been disabled on profile level');
        
        triggerActivation.SetupOwnerId = UserInfo.getOrganizationId();
        update triggerActivation;        
        triggersActivated = TriggerHandler.triggersActivated();
        System.assertEquals(false, triggersActivated, 'Triggers have not been disabled on organization level');
        
        delete triggerActivation;
        triggersActivated = TriggerHandler.triggersActivated();
        System.assertEquals(true, triggersActivated, 'Triggers have not been activated');
        
        Test.stopTest();        
    }    

    
    /**
    * @description Test the trigger handler framework
    * @return void 
    **/
    @IsTest
    public static void TriggerHandlerTest() {                               
        System.Test.startTest();
        
        TriggerFrameworkHandlerTest triggerContext = generateTriggerContext(true, true, false, false, false);
        triggerContext.run();
        System.assert(GlobalUtilsConstants.onBeforeInsertRun, 'Before insert trigger did not successfully run');
        
        triggerContext = generateTriggerContext(false, true, false, false, false);
        triggerContext.run();
        System.assert(GlobalUtilsConstants.onAfterInsertRun, 'After insert trigger did not successfully run');
        
        triggerContext = generateTriggerContext(true, false, true, false, false);
        triggerContext.run();
        System.assert(GlobalUtilsConstants.onBeforeUpdateRun, 'Before update trigger did not successfully run');
        
        triggerContext = generateTriggerContext(false, false, true, false, false);
        triggerContext.run();
        System.assert(GlobalUtilsConstants.onAfterUpdateRun, 'After update trigger did not successfully run');
        
        triggerContext = generateTriggerContext(true, false, false, true, false);
        triggerContext.run();
        System.assert(GlobalUtilsConstants.onBeforeDeleteRun, 'Before delete trigger did not successfully run');
        
        triggerContext = generateTriggerContext(false, false, false, true, false);
        triggerContext.run();
        System.assert(GlobalUtilsConstants.onAfterDeleteRun, 'After delete trigger did not successfully run');
        
        triggerContext = generateTriggerContext(false, false, false, false, true);
        triggerContext.run();
        System.assert(GlobalUtilsConstants.onAfterUndeleteRun, 'After undelete trigger did not successfully run');
                
        System.Test.stopTest();                
    }
    
    /**
    * @description Generate trigger context used by the test classes
    * @param Boolean isBefore 
    * @param Boolean isInsert 
    * @param Boolean isUpdate 
    * @param Boolean isDelete 
    * @param Boolean isUndelete 
    * @return TriggerFrameworkHandlerTest 
    **/
    private static TriggerFrameworkHandlerTest generateTriggerContext(Boolean isBefore, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete){
        TriggerFrameworkHandlerTest triggerContext = new TriggerFrameworkHandlerTest();        
        
        triggerContext.isBefore = isBefore;
        triggerContext.isAfter = isBefore ? false : true;
        
        triggerContext.isInsert = isInsert;
        triggerContext.isUpdate = isUpdate;
        triggerContext.isDelete = isDelete;
        triggerContext.isUndelete = isUndelete;
        
        return triggerContext;        
    }
}