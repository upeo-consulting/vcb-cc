/**
 * Created by Frederik on 3/08/2021.
 */

public interface IAccountRecordSelector {

    List<Asset> getRelatedAssets(List<Id> accIds);
    //List<Opportunity> getRelatedOpportunities(List<Id> accIds);
    List<Quote> getRelatedQuotes(List<Id> accIds);

}