public with sharing class QuoteSelector implements IQuoteSelector {
    
    public List<Quote> getAccountRecordTypeByQuoteId (List<String> quoteIds) {
        
        List<Quote> queryResult = [SELECT Id, 
                                   Opportunity.Account.RecordType.DeveloperName, 
                                   Opportunity.AccountId, 
                                   Opportunity.Account.IsPersonAccount, 
                                   Opportunity.Account.PersonContactId, 
                                   Opportunity.Account.Email__c  
                                   FROm Quote 
                                   WHERE Id in: quoteIds];             
        return queryResult;
        
    }

    public List<AccountContactRelation> getContactsByEmail (List<Quote> quotes) {
        
		List<String> quoteAccounts = new List<String>();
        List<String> quoteEmails = new List<String>();
        Map<String, Contact> mapContactEmails = new Map<String, Contact>();
        for (Quote recQuote : quotes) {
            quoteAccounts.add(recQuote.Opportunity.AccountId);
            quoteEmails.add(recQuote.Opportunity.Account.Email__c);
        }
        
        List<AccountContactRelation> contactQueryResult = [ Select Id, AccountId, Contact.Email_Work__c
                                            FROM AccountContactRelation
                                           	WHERE AccountId in: quoteAccounts and Contact.Email_Work__c in: quoteEmails];

        return contactQueryResult;
    
    }

    public list<Asset> getQuoteAssets(List<String> quoteIds) {

        List<Asset> assetList = [Select Quote__c, Make__c from Asset where Quote__r.Id in :quoteids];
        return assetList;
    }

    
}