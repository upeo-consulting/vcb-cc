/**
 * @Version: 1.0
 * @Author: Frederik Pardon
 * @Copyright: 2021 4C Consulting/Wipro
 * @Uses:
 *
 * -----------------------------------------------------------------------------------------------
 * Description: Any logic that is needed for Asset should be implemented in this class.
 *
 *
 * Created: 02/04/2021
 * Last Updated: 02/04/2021
 *
 * Unit tests: 
 *
 * Change log:
 * -----------------------------------------------------------------------------------------------
 */
 
 public with sharing class AssetService{
 
 
     IAssetSelector assetSelector;
    
     public AssetService(IAssetSelector assetSelector) {
        this.assetSelector = assetSelector;

    }
    
    public AssetService() {
    }

    public void removeContactFromAsset (Map<Id, Asset> newAssets, Map<Id, Asset> oldAssets) {

        for(Asset asst : newAssets.values()){

            if(asst.AccountId != oldAssets.get(asst.Id).AccountId){

                asst.ContactId = null;

            }

        }

    }
    
    public void assetRelationHandler(Map<Id, Asset> newAssets, Map<Id, Asset> oldAssets) {
    
    Map<Id, Asset> onlyChangedAssets = new Map<Id, Asset>();
    
    for (Asset newAsset : newAssets.values()){
        
        Asset oldAsset = oldAssets.get(newAsset.Id);
        
        if(newAsset.Leasing_Company__c != oldAsset.Leasing_Company__c  || newAsset.AccountId != oldAsset.AccountId 
        || newAsset.Retailer__c  != oldAsset.Retailer__c) {
            
            onlyChangedAssets.put(newAsset.Id, newAsset);
            
            }
        
        }
        
    addAccountRelationsToAccount(onlyChangedAssets);
    
    }


    // Method that updates the Account_Relations__c based on the lookup fields on Asset
    public void addAccountRelationsToAccount(Map<Id, Asset> newAssets) {
         
         List<Account_Relationship__c> listAccountRelations = new List<Account_Relationship__c>();
         List<AccountContactRelation> listAccountContactRelations = new list<AccountContactRelation>();

         List<String> listAssetsToQuery = new List<String>();
         
         for (Asset objAsset : newAssets.values()) {
            listAssetsToQuery.add(objAsset.Id);
            }
 
        List<Asset> listAssetRelations = assetSelector.getAssetsWithRelations(listAssetsToQuery);

                         
        for (Asset objAsset : listAssetRelations) {

        if (objAsset.AccountId != null) {    
         
             // Checks which logic to execute based on which fields on Asset are filled out             
             // Upsert Account_Relations__c if the Leasing Company is filled out
             if (objAsset.Leasing_Company__c != null) {
				 system.debug('! Adding Leasing Company');
                 Account_Relationship__c accountRelation = new Account_Relationship__c();
                 accountRelation.External_Id__c = objAsset.Account.External_Id__c + objAsset.Leasing_Company__r.External_Id__c;
                 accountRelation.Account_From__c = objAsset.AccountId;
                 accountRelation.Account_To__c = objAsset.Leasing_Company__c ;
                 accountRelation.Role__c = 'Leasing Company';
                 accountRelation.Tenant_Id__c = objAsset.Tenant_Id__c;
                 system.debug(accountRelation);
                 listAccountRelations.add(accountRelation);
                 
            }
            
            // Upsert Account_Relations__c if the Retailer is filled out
            if (objAsset.Retailer__c != null) {
                 system.debug('! Adding Retailer');          
                 Account_Relationship__c accountRelation = new Account_Relationship__c();
                 accountRelation.External_Id__c = objAsset.Account.External_Id__c + objAsset.Retailer__r.External_Id__c;
                 accountRelation.Account_From__c = objAsset.AccountId;
                 accountRelation.Account_To__c = objAsset.Retailer__c;
                 accountRelation.Tenant_Id__c = objAsset.Tenant_Id__c;
                 accountRelation.Role__c = 'Retailer';
                 listAccountRelations.add(accountRelation);
                 
            }
            // Upsert AccountContactRelations if the Contact is filled out
            /*if (objAsset.ContactId != null && objAsset.AccountId != null && objAsset.Account.IsPersonAccount == false) {
                system.debug('! Adding Contact');
                AccountContactRelation accountContactRelation = new AccountContactRelation();
                accountContactRelation.External_Id__c = objAsset.Account.External_Id__c + objAsset.Contact.External_Id__c;
                accountContactRelation.AccountId = objAsset.AccountId;
                accountContactRelation.ContactId = objAsset.ContactId;
                accountContactRelation.Roles = 'Employer';
                accountContactRelation.Tenant_Id__c = objAsset.Tenant_Id__c;
                listAccountContactRelations.add(accountContactRelation);
                
            }
                */
            //upsert listAccountContactRelations External_Id__c;
            upsert listAccountRelations External_Id__c;
        }

        }
          
    }
    
    
    public void addAssetPersonContactUpdateHandler(Map<Id, Asset> newAssets, Map<Id, Asset> oldAssets) {
    
        Map<Id, Asset> onlyChangedAssets = new Map<Id, Asset>();
        
        for (Asset newAsset : newAssets.values()) {
        
            Asset oldAsset = oldAssets.get(newAsset.Id);
            
            if (newAsset.AccountId != oldAsset.AccountId && newAsset.AccountID != null) {
            onlyChangedAssets.put(newAsset.Id, newAsset);            
            }
        
        }
        
        addAssetPersonContact(onlyChangedAssets);
    
    }
    
    public void addAssetPersonContact(Map<Id, Asset> newAssets) {
    
        List<String> assetsToQuery = new List<String>();
        List<Asset> assetsToUpdate = new List<Asset>();
        
        for (Asset asset : newAssets.values()) {
            
            if(asset.AccountId != null) {

            String accountId = asset.AccountId;
            assetsToQuery.add(accountId);
            }
        }
        
        List<Asset> assetsToProcess = assetSelector.getAssetsWithAccount(assetsToQuery);
        
        for(Asset assetToProcess : assetsToProcess) {
        
            if(assetToProcess.Account.RecordType.DeveloperName == 'PersonAccount') {
                assetToProcess.ContactId = assetToProcess.Account.PersonContactId;
                assetsToUpdate.add(assetToProcess);
                system.debug('PersonContactId updated');
            }
        
        }
        
        update(assetsToUpdate);
        
    
    }
    
    
    
    //Get and assign Golden Asset Records
    public void addGoldenAsset(Map<Id, Asset> newAssets) {
        
        List<String> listGoldenAssetsToQuery = new List<String>();
        List<Id> listStandardAssetsToQuery = new List<Id>();
        List<Asset> listStandardAsset = new list<Asset>();
        List<Asset> listGoldenAsset = new list<Asset>();
        
        for (Asset objAsset : newAssets.values()) {
            listGoldenAssetsToQuery.add(objAsset.Vin__c);
            listStandardAssetsToQuery.add(objAsset.Id);
            }
 
                 
        List<Asset> listGoldenAssets = assetSelector.getAssetByVIN(listGoldenAssetsToQuery);
        List<Asset> listAssetsToUpdate = assetSelector.getAssetsById(listStandardAssetsToQuery);
        
        Map<String, Asset> mapAssetVin = new map<String, Asset>();
        
        for(Asset ast : listGoldenAssets) {
            mapAssetVin.put(ast.VIN__c, ast);
        }
        
       
        Asset objAssetToUpdate = new Asset();
        
        Id recordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Asset', 'Asset_Golden_Record');    
               
            for (Asset standardAsset: listAssetsToUpdate) {
                                              
                if (standardAsset.VIN__c != null && mapAssetVin.containsKey(standardAsset.VIN__c) == true && standardAsset.Ultimate_Parent__c ==  null) {
                    system.debug('Executing Golden Record Linking logic');
                    Asset goldenAsset = mapAssetVin.get(standardAsset.VIN__c);
                    standardAsset.Ultimate_Parent__c = goldenAsset.Id;
                    listStandardAsset.add(standardAsset);
                }
                
                if (mapAssetVin.containsKey(standardAsset.VIN__c) == false && standardAsset.Ultimate_Parent__c ==  null) {
                    system.debug('Executing Golden Record Creation logic');
                    Asset objGoldenAsset = new Asset();
                    Account accountReference = new Account(External_Id__c = 'VCB');
                       
                    objGoldenAsset.Name = standardAsset.Name + ' - Golden';
                    //objGoldenAsset.Account = accountReference;
                    objGoldenAsset.VIN__c = standardAsset.VIN__c;
                    objGoldenAsset.Make__c = standardAsset.Make__c;
                    objGoldenAsset.Model__c = standardAsset.Model__c;
                    objGoldenAsset.Type__c = standardAsset.Type__c;
                    objGoldenAsset.External_Id__c = standardAsset.VIN__c + '_Golden';
                    objGoldenAsset.RecordTypeId = recordTypeId;
                       
                    Asset ultimateParent = new Asset(External_Id__c = objGoldenAsset.External_Id__c);
                        
                    standardAsset.Ultimate_Parent__r = ultimateParent;
                        
                    listGoldenAsset.add(objGoldenAsset);
                    listStandardAsset.add(standardAsset);
                  }
                        
            }

                if (listGoldenAsset.size() > 0) {
                    upsert listGoldenAsset External_Id__c;
                }  

                if (listStandardAsset.size() > 0) {
                    update listStandardAsset;
                }
        
    }
}