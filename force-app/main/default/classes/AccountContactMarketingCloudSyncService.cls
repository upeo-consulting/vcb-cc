/**
 * @Version: 1.0
 * @Author: Frederik Pardon
 * @Copyright: 2021 4C Consulting/Wipro
 * @Uses:
 *
 * -----------------------------------------------------------------------------------------------
 * Description: Any logic that is needed for Accounts should be implemented in this class.
 *
 *
 * Created: 20/04/2021
 * Last Updated: 20/04/2021
 *
 * Unit tests:
 *
 * Change log:
 * -----------------------------------------------------------------------------------------------
 */

public with sharing class AccountContactMarketingCloudSyncService {

    public AccountContactMarketingCloudSyncService() {

    }

    // method that sets the checkboxes on Account that are used to sync data to Marketing Cloud

    public void setMarketingCloudCheckboxes(map<Id, Sobject> newMap) {
        
        system.debug('SyncBox Class was called');
        
        List<SObject> listRecordsToUpdate = new List<SObject>();

        Schema.SObjectType sObjectType = newMap.values()[0].getSObjectType();
        String objectType = String.valueOf(sObjectType);
        system.debug(objectType);

        system.debug('Executing setMarketingCloudCheckboxes');

        List<MarketingCloudSyncBox__mdt> listSyncBoxes = MarketingCloudSyncBox__mdt.getAll().values();
        Map<String, MarketingCloudSyncBox__mdt> mapSyncBoxes = new Map<String, MarketingCloudSyncBox__mdt>();

        //Create map for all MarketingCloudSyncBox CMD records
        for (MarketingCloudSyncBox__mdt syncBox : listSyncBoxes) {
            mapSyncBoxes.put(syncBox.Record_Type_DeveloperName__C, syncBox);
        }

        //Checks every record in newMap and compares the field with what is stored in the mapSyncBoxes Map.
        // The logic is driven by the record type of the Account.
        for (SObject record : newMap.values()) {
            
            String recordTypeDevName = '';
            
            if(record.get('isPersonAccount') == TRUE && objectType == 'Account') {
                Boolean isPersonAccount = TRUE;
            }
            
            recordTypeDevName = GlobalUtilsHelper.getRecordTypeDeveloperName(objectType, (String) record.get('RecordTypeId'));
            system.debug('recordTypeDevName = ' +recordTypeDevName);
                
            MarketingCloudSyncBox__mdt customMetadataRecord = mapSyncBoxes.get(recordTypeDevName);
            system.debug(customMetadataRecord);
              
    
            // Only execute if a custom metadata record was retrieved
            if (customMetadataRecord != null) {

                //Split the Field_To_Update so that the fields can be used for the updates
                List<String> fieldsToUpdate = customMetadataRecord.Field_To_Update__c.split(',');
                //Split the Email_Field__c to a list so if multiple fields are defined on the CMD record they can all be checked
                List<String> fieldToCheck = new List<String>();
                if (customMetadataRecord.Email_Field__c != null) {                
                    fieldToCheck = customMetadataRecord.Email_Field__c.split(',');
                    }
                //Set the listsize so we can use it tod decide which branch to follow in the logic.
                String environmentCheck = customMetadataRecord.Environment_Check__c;

                Integer listSize = fieldToCheck.size();

                system.debug('list size = ' +listSize);
                system.debug('fieldToCheck = ' + fieldToCheck);

                //If the list contains only one value, we only need to compare one field
                //we use a for loop to loop through all the fields contained in splitEmailfield and compare them to the record values

                Integer i = 0;
                if(listSize == 0) {
                        system.debug('Marketing Sync Allowed, no email criteria defined');
                        SObject recordToUpdate = Schema.getGlobalDescribe().get(objectType).newSObject() ;

                        // If Environment_Check__MC__c is TRUE and fieldToUpdate was set to FALSE, we also set Sync_To_MC__c to FALSE

                            for (String updateField : fieldsToUpdate) {
                                updateField = updateField.trim();
                                recordToUpdate.put(updateField, TRUE);
                                recordToUpdate.put('Id', record.Id);
                            }

                        listRecordsToUpdate.add(recordToUpdate);

                }
                
                if(listSize > 0) {
                
                    while (i <= listSize-1) {
    
                        // if the CMD record is not null and the values to compare on the record is not null we update the checkbox to TRUE
                        if (fieldToCheck[i].trim() != null & record.get(fieldToCheck[i].trim()) != null ) {
                            system.debug('Marketing Sync Allowed');
                            SObject recordToUpdate = Schema.getGlobalDescribe().get(objectType).newSObject() ;
    
                            // If Environment_Check__MC__c is TRUE and fieldToUpdate was set to TRUE, we also set Sync_To_MC__c to TRUE
                            if (record.get(environmentCheck) == TRUE) {
                                for (String updateField : fieldsToUpdate) {
                                    updateField = updateField.trim();
                                    recordToUpdate.put(updateField, TRUE);
                                    recordToUpdate.put('Id', record.Id);
                                }
                            }
    
                            listRecordsToUpdate.add(recordToUpdate);
                            break;
                        }
    
                        // if the CMD record is not null and the values to compare on the record is not null we update the checkbox to FALSE
                        if (fieldToCheck[i].trim() != null & record.get(fieldToCheck[i].trim()) == null) {
                            system.debug('Marketing Not Sync Allowed');
                            SObject recordToUpdate = Schema.getGlobalDescribe().get(objectType).newSObject() ;
    
                            // If Environment_Check__MC__c is TRUE and fieldToUpdate was set to FALSE, we also set Sync_To_MC__c to FALSE
    
                            for (String updateField : fieldsToUpdate) {
                                updateField = updateField.trim();
                                recordToUpdate.put(updateField, FALSE);
                                recordToUpdate.put('Id', record.Id);
                            }
    
                            listRecordsToUpdate.add(recordToUpdate);
                        }            
                        i++;
                    }
                }
            }
        }

            //We create a set and a list to dedupe any duplicated records in listRecordToUpdate
            List<SObject> deDupeList = new List<SObject>();
            Map<Id, SObject> mapSOBJ = new Map<Id, SObject>();
            mapSOBJ.putAll(listRecordsToUpdate);
            deDupeList.addAll(mapSOBJ.values());
            //We update the records in deDupeList
           system.debug(deDupeList);
           update deDupeList;

    }
}