public with sharing class DuplicateService {

    IDuplicateSelector duplicateSelector;

    public DuplicateService(IDuplicateSelector duplicateSelector) {
        this.duplicateSelector = duplicateSelector;
    }

    public DuplicateService() {

    }

    // method that checks whether a record with the same external id already exists
    public void checkDuplicateRecords(map<Id, Sobject> newMap) {

        List<String> externalIds = new List<String>();
        List<String> ids = new List<String>();

        Schema.SObjectType sObjectType = newMap.values()[0].getSObjectType();
        String typeName = String.valueOf(sObjectType);


        for(Sobject record : newMap.values()) {

            if (record.get('External_Id__c') != null) {
                String extId = (String) record.get('External_Id__c');
                externalIds.add(extId);
                ids.add(record.id);
            }
        }

        List<Sobject> recordsToProcess = duplicateSelector.getDuplicateRecords(externalIds, ids, typeName);
        system.debug(recordsToProcess);

        Map<String, String> recordMap = new Map<String, String>();
        for (SObject returnRecord : recordsToProcess) {
            String extId = string.valueOf(returnRecord.get('External_Id__c'));
            recordMap.put(extId, returnRecord.Id);
        }

        List<String> recordsToDelete = new List<String>();

        for (SObject record : newMap.values()) {
            String newExternalId = string.valueOf(record.get('External_Id__c'));
            if(recordMap.get(newExternalId) != null) {
                recordsToDelete.add(record.id);
            }
        }

        delete duplicateSelector.getRecordsToDelete(recordsToDelete, typeName);



    }

}