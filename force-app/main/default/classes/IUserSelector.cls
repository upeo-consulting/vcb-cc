/**
 * Created by Frederik Pardon on 07/04/2020.
 */

public interface IUserSelector {
    
    List<User> getOwnerByTenant (list<String> tenants);

}