/**
 * Created by Frederik Pardon on 07/04/2021.
 */

public with sharing class AccountRelationTriggerHandler extends TriggerHandler {
    
        protected override void onAfterInsert(Map<Id, SObject> newMap) {
                IUserSelector userSelector = (IUserSelector) new UserSelector();
                IMCSyncSelector mCSyncSelector = (IMCSyncSelector) new MCSyncSelector();
                OwnerService ownerService = (OwnerService) new OwnerService(userSelector);
                MarketingCloudSyncService marketingCloudSyncService = (MarketingCloudSyncService) new MarketingCloudSyncService(mCSyncSelector);

            if(RecursiveTriggerHandler.isFirstTime == true) {
                RecursiveTriggerHandler.isFirstTime = false;
                ownerService.setRecordOwner((map<Id, SObject>) newMap, null);
                marketingCloudSyncService.setCheckBoxes((map<Id, SObject>) newMap);
            }


        }

       protected override void onAfterUpdate(Map<Id, SObject> newMap, Map<Id, SObject> oldMap) {
                IUserSelector userSelector = (IUserSelector) new UserSelector();
                IMCSyncSelector mCSyncSelector = (IMCSyncSelector) new MCSyncSelector();
 				MarketingCloudSyncService marketingCloudSyncService = (MarketingCloudSyncService) new MarketingCloudSyncService(mCSyncSelector);
                OwnerService ownerService = (OwnerService) new OwnerService(userSelector);
           
           if(RecursiveTriggerHandler.isFirstTime == true) {
               RecursiveTriggerHandler.isFirstTime = false;
               ownerService.setRecordOwner((map<Id, SObject>) newMap, (Map<Id, SObject>) oldMap);
               marketingCloudSyncService.setCheckBoxes((map<Id, SObject>) newMap);
               
           }
     }
}