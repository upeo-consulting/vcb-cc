public with sharing class AssetTriggerHandler extends TriggerHandler{
    
    
    protected override void onAfterInsert(Map<Id, SObject> newMap) {
        IAssetSelector assetSelector = (IAssetSelector) new AssetSelector();
        AssetService assetService = (AssetService) new AssetService(assetSelector);
        IUserSelector userSelector = (IUserSelector) new UserSelector();
        OwnerService ownerService = (OwnerService) new OwnerService(userSelector);
        IMCSyncSelector mCSyncSelector = (IMCSyncSelector) new MCSyncSelector();
        IDuplicateSelector duplicateSelector = (DuplicateSelector) new DuplicateSelector();
        MarketingCloudSyncService marketingCloudSyncService = (MarketingCloudSyncService) new MarketingCloudSyncService(mCSyncSelector);
        DuplicateService duplicateService = (DuplicateService) new DuplicateService(duplicateSelector);


       	if(RecursiveTriggerHandler.isFirstTime || Test.isRunningTest()) {
			RecursiveTriggerHandler.isFirstTime = false;
            assetService.addAccountRelationsToAccount((Map<Id, Asset>) newMap);
            assetService.addGoldenAsset((Map<Id, Asset>) newMap);
            assetService.addAssetPersonContact((Map<Id, Asset>) newMap);
            ownerService.setRecordOwner((map<Id, SObject>) newMap, null);
            marketingCloudSyncService.setCheckBoxes((map<Id, SObject>) newMap);
            duplicateService.checkDuplicateRecords((map<Id, SObject>) newMap);
        }

    }

    protected override void onBeforeUpdate(Map<Id, SObject> newMap, Map<Id, SObject> oldMap) {
        IAssetSelector assetSelector = (IAssetSelector) new AssetSelector();
        AssetService assetService = (AssetService) new AssetService(assetSelector);
        assetService.removeContactFromAsset((Map<Id, Asset>) newMap, (Map<Id, Asset>) oldMap);

    }

    protected override void onAfterUpdate(Map<Id, SObject> newMap, Map<Id, SObject> oldMap) {
    
        IAssetSelector assetSelector = (IAssetSelector) new AssetSelector();
        IUserSelector userSelector = (IUserSelector) new UserSelector();
        AssetService assetService = (AssetService) new AssetService(assetSelector);
        IMCSyncSelector mCSyncSelector = (IMCSyncSelector) new MCSyncSelector();
        OwnerService ownerService = (OwnerService) new OwnerService(userSelector);
        MarketingCloudSyncService marketingCloudSyncService = (MarketingCloudSyncService) new MarketingCloudSyncService(mCSyncSelector);

        if(RecursiveTriggerHandler.isFirstTime || Test.isRunningTest()) {
            RecursiveTriggerHandler.isFirstTime = false;
            ownerService.setRecordOwner((map<Id, SObject>) newMap, (Map<Id, Asset>) oldMap);
            assetService.addAssetPersonContactUpdateHandler((Map<Id, Asset>) newMap, (Map<Id, Asset>) oldMap);
            assetService.assetRelationHandler((Map<Id, Asset>) newMap, (Map<Id, Asset>) oldMap);
            marketingCloudSyncService.setCheckBoxes((map<Id, SObject>) newMap);
        }
    
    }
}