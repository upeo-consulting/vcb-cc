/**
 * Created by Frederik Pardon on 02/04/2020.
 */

public interface IAssetSelector {
    List<Asset> getAssetsById(List<Id> assetIds);
    List<Asset> getAssetByVIN(List<String> VIN);
    List<Asset> getAssetsWithRelations(list<String> AssetRelations);
    List<Asset> getAssetsWithAccount(list<String> accountIds);
}