/**
 * Created by Frederik Pardon on 07/04/2021.
 */

public with sharing class ContactTriggerHandler extends TriggerHandler {

        protected override void onAfterInsert(Map<Id, SObject> newMap) {
                IUserSelector userSelector = (IUserSelector) new UserSelector();
                OwnerService ownerService = (OwnerService) new OwnerService(userSelector);
                IDuplicateSelector duplicateSelector = (DuplicateSelector) new DuplicateSelector();
                DuplicateService duplicateService = (DuplicateService) new DuplicateService(duplicateSelector);
                AccountContactMarketingCloudSyncService accountContactMarketingCloudSyncService = (AccountContactMarketingCloudSyncService) new AccountContactMarketingCloudSyncService();
                
                system.debug('RecursiveTriggerHandler.isFirstTime = ' + RecursiveTriggerHandler.isFirstTime + ' when ContactTrigger Was Called');

                if(RecursiveTriggerHandler.isFirstTime == true) {
                        RecursiveTriggerHandler.isFirstTime = false;
                        ownerService.setRecordOwner((map<Id, SObject>) newMap, null);
                        accountContactMarketingCloudSyncService.setMarketingCloudCheckboxes((map<Id, SObject>) newMap);
                        duplicateService.checkDuplicateRecords((map<Id, SObject>) newMap);
                        

                        
                }


        }

        protected override void onAfterUpdate(Map<Id, SObject> newMap, Map<Id, SObject> oldMap) {
                IUserSelector userSelector = (IUserSelector) new UserSelector();
                OwnerService ownerService = (OwnerService) new OwnerService(userSelector);
                AccountContactMarketingCloudSyncService accountContactMarketingCloudSyncService = (AccountContactMarketingCloudSyncService) new AccountContactMarketingCloudSyncService();

                if(RecursiveTriggerHandler.isFirstTime == true) {
                        RecursiveTriggerHandler.isFirstTime = false;
                        ownerService.setRecordOwner((map<Id, SObject>) newMap, (Map<Id, SObject>) oldMap);    
                        accountContactMarketingCloudSyncService.setMarketingCloudCheckboxes((map<Id, SObject>) newMap);
                        
                 

                }
        }

}