/**
 * Created by Frederik on 3/08/2021.
 */

public with sharing class AccountService {

    IAccountRecordSelector accountRecordSelector;

    public AccountService(IAccountRecordSelector accountRecordSelector) {
        this.accountRecordSelector = accountRecordSelector;
    }

    public AccountService(){

    }

    public void relatedRecordHandler(Map<Id, Account> newAccounts, Map<Id, Account> oldAccounts) {

        Map<Id, Account> onlyChangedAccounts = new Map<Id, Account>();
        Map<Id, Account> onlyChangedAccountsMCFalse = new Map<Id, Account>();
        List<Id> accIds = new List<Id>();
        List<Id> accIdsMCFalse = new List<Id>();

        for (Account newAccount : newAccounts.values()) {

            if (newAccount.Sync_to_MC__c == TRUE) {
                onlyChangedAccounts.put(newAccount.Id, newAccount);
                accIds.add(newAccount.Id);
            }

            if (newAccount.Sync_to_MC__c == FALSE) {
                onlyChangedAccountsMCFalse.put(newAccount.Id, newAccount);
                accIdsMCFalse.add(newAccount.Id);
                system.debug('Asset now allowed in MC');
            }

        }

        List<Asset> assetRecords = accountRecordSelector.getRelatedAssets(accIds);
        List<Asset> assetRecordsMCFalse = accountRecordSelector.getRelatedAssets(accIdsMCFalse);

        system.debug('asset list size = ' +assetRecords.size());
        system.debug('asset not allowed list size = ' +assetRecordsMCFalse.size());

        for(Asset ass : assetRecords) {
            ass.Sync_to_MC__c = TRUE;
        }

        for(Asset ass : assetRecordsMCFalse) {
            ass.Sync_to_MC__c = FALSE;
        }

       List<Quote> quoteRecords = accountRecordSelector.getRelatedQuotes(accIds);
       List<Quote> quoteRecordsMCFalse = accountRecordSelector.getRelatedQuotes(accIdsMCFalse);

       system.debug('quote list size = ' +quoteRecords.size());

        for(Quote qte : quoteRecords){
            qte.Sync_to_MC__c = TRUE;
        }

        for(Quote qte : quoteRecordsMCFalse){
            qte.Sync_to_MC__c = FALSE;
        }

        update assetRecords;
        update assetRecordsMCFalse;
        update quoteRecords;
        update quoteRecordsMCFalse;




    }

}