/**
 * Created by Frederik on 7/05/2021.
 */

public with sharing class QuoteService {

    
    IQuoteSelector quoteSelector;
    IContactSelector contactSelector;

    public QuoteService(IContactSelector contactSelector, IQuoteSelector quoteSelector) {
        this.contactSelector = contactSelector;
        this.quoteSelector = quoteSelector;
    }



    public QuoteService() {

    }

    public void setQuoteContact(Map<Id, Quote> newmap) {
        system.debug('setQuoteContact Class was called');
        
        List<String> quoteIds = new List<String>();
        List<Quote> personAccountQuotes = new List<Quote>();
        List<Quote> standardAccountQuotes = new List<Quote>();
        
                
        for(Quote recQuote : newmap.values()) {
            quoteIds.add(recQuote.Id);
        }
              
        List<Quote> listQuote = quoteSelector.getAccountRecordTypeByQuoteId(quoteIds);
        
        for(Quote recQuote : listQuote) {
            if(recQuote.Opportunity.Account.IsPersonAccount == TRUE) {
                recQuote.ContactId = recQuote.Opportunity.Account.PersonContactId;
                personAccountQuotes.add(recQuote);
            }
            if(recQuote.Opportunity.Account.IsPersonAccount == FALSE) {
            	standardAccountQuotes.add(recQuote);
            }
        }
        
        system.debug('personAccountQuotes = ' +personAccountQuotes);
        system.debug('standardAccountQuotes = ' +standardAccountQuotes);
        
        List<AccountContactRelation> contactEmailList = quoteSelector.getContactsByEmail(standardAccountQuotes);
                
        Map<String, Integer> mapContactEmails = new Map<String, Integer>();
        
        for (Quote quote : standardAccountQuotes ) {
            integer i = 0;
            String contactId;
            for (AccountContactRelation acr : contactEmailList) {
            
                if (quote.Opportunity.Account.Email__c == acr.Contact.Email_Work__c & quote.Opportunity.AccountId == acr.AccountId) {
                    i++;
                }
                if (i == 1) {
                    contactId = acr.ContactId;                    
                }
            }
            
            system.debug(i);
            if (i == 1) {
                quote.ContactId = contactId;
            }
        }
        system.debug('standardAccountQuotes = ' +standardAccountQuotes);
        system.debug('mapContactEmails = ' +mapContactEmails);
        
        update personAccountQuotes;
        update standardAccountQuotes;
               
    }

    public void setSelektCarFields(Map<Id, Quote> newMap) {

        system.debug('!!!! setSelektCarFields was called');

        list<String> quoteIds = new list<String>();
        list<Quote> quotesToProcess = new list<Quote>();

        for (Quote qte : newMap.values()) {

            if (qte.VIN_SelektCar__c != null && qte.Make__c == null) {
                quoteIds.add(qte.Id);
                quotesToProcess.add(qte);
            }
        }

        List<Asset> listAsset = quoteSelector.getQuoteAssets(quoteIds);

        Map<Id, String> mapAssetMake = new Map <Id, String>();

        for(Asset asst : listAsset) {
            Id quoteId = asst.Quote__c;
            String make = asst.Make__c;
            mapAssetMake.put(quoteId, make);
        }

        List<Quote> updateList = new List<Quote>();

        for(Quote qte : quotesToProcess) {
                system.debug('!!! processed');
                Quote newQte = new Quote();
                newQte.Id = qte.Id;
                newQte.Make__c = mapAssetMake.get(qte.Id);
                updateList.add(newQte);
        }

//        system.debug(quotesToUpdate);
          update updateList;
    }
        
            
}