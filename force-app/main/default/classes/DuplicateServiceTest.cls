/**
 * Created by Frederik on 1/07/2021.
 */

@IsTest
private class DuplicateServiceTest {

    @TestSetup
    static void testData() {

        Id accountRecordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Account', 'Standard_Account');
        List<Account> accList = new List<Account>();
        List<Opportunity> oppList = new List<Opportunity>();
        List<Quote> quoteList = new List<Quote>();

        for(Integer i = 0; i < 10;i++){
            Account a = new Account();
            a.Name = 'TestAccount'+i;
            a.RecordTypeId = accountRecordTypeId;
            a.Type = 'Customer';
            a.External_Id__c = 'ExtId'+i;
            accList.add(a);

            Opportunity opp = new Opportunity();
            opp.Name = 'TestOpp'+i;
            opp.AccountId = a.Id;
            opp.StageName = 'Open';
            opp.CloseDate = System.today();
            opp.External_Id__c = 'ExtId'+i;
            oppList.add(opp);


        }

        insert accList;
        insert oppList;
        
        Integer i = 0;

        for (Opportunity opp : [SELECT Id FROM Opportunity WHERE Name LIKE 'TestOpp%']) {
            Quote q = new Quote();
            q.OpportunityId = opp.id;
            q.Name = 'TestQuote'+i;
            q.External_Id__c = 'ExtId'+i;
            i++;
            quoteList.add(q);
        }

        insert quoteList;

    }

    @IsTest
    static void testDuplicateQuotes() {

        //Insert list of new Quotes with External Ids that are already in the database

        List<Quote> qteList = [SELECT Id, External_Id__c, OpportunityId FROM Quote WHERE Name LIKE 'TestQuote%'];
        List<Quote> newQuotes = new List<Quote>();

        for(Quote qte : qteList) {
            Quote newQuote = new Quote();
            Integer i = 0;
            newQuote.OpportunityId = qte.OpportunityId;
            newQuote.Name = 'NewTestQuote'+i;
            newQuote.External_Id__c = 'ExtId'+i;
            newQuotes.add(newQuote);
        }

        insert newQuotes;

        //Expected behaviour is that the new quotes have not been created in SFDC
        List<Quote> quotesToTest = [SELECT Id, External_Id__c, OpportunityId FROM Quote WHERE Name LIKE 'NewTestQuote%'];
        system.assertEquals(0, quotesToTest.size());

    	}

    	@IsTest
    	static void testDuplicateOppties() {        
        
        List<Opportunity> opptyList = [SELECT Id, AccountId, External_Id__c FROM Opportunity WHERE Name LIKE 'TestOpp%'];
        List<Opportunity> newOppties = new List<Opportunity>();
        
        Test.startTest();
            
        Integer i = 0;

        for (Opportunity opp : opptyList) {
            Opportunity newOpp = new Opportunity();
            newOpp.AccountId = opp.AccountId;
            newOpp.Name = 'NewTestOpp'+i;
            newOpp.StageName ='Open';
            newOpp.CloseDate = system.today();
            newOpp.External_Id__c = opp.External_Id__c;
            newOppties.add(newOpp);
            i++;
        }

        insert newOppties;
            
        Test.stopTest();

        //Expected behaviour is that the new quotes have not been created in SFDC
        List<Opportunity> opptiesToTest = [SELECT Id FROM Opportunity WHERE Name LIKE 'NewTestOpp%'];
        system.assertEquals(0, opptiesToTest.size());


    }
}