/**
 * Created by Frederik Pardon on 02/04/2021.
 */

public with sharing class AssetSelector implements IAssetSelector {


    public static List<Asset> getAssetsWithAccount(List<String> accountIds) {
    
        return [SELECT Id, AccountId, ContactId, Account.PersonContactId, RecordTypeId, Account.RecordType.DeveloperName
               FROM Asset
               WHERE AccountId in :accountIds
               ];
    
    }


    public static List<Asset> getAssetsById(List<String> Ids) {
        return [
                SELECT Id, Name, Model__c, Make__c, VIN__c, Type__c, Ultimate_Parent__c, External_Id__c
                       From Asset
                       WHERE Id In :Ids
                       AND RecordType.DeveloperName = 'Standard_Asset'
        ];
    }

    public static List<Asset> getAssetByVIN(List<String> VIN) {
        return [
                SELECT Id, Name, AccountId, VIN__c, Make__c, Model__c, External_Id__c 
                FROM Asset
                WHERE VIN__c IN :VIN
                AND RecordType.DeveloperName = 'Asset_Golden_Record'
        ];
    }

    public static List<Asset> getAssetsWithRelations(list<String> AssetRelations) {
        return [
                Select 
                    Id,
                    AccountId,
                    ContactId, 
                    Account.External_Id__c,
                    Contact.External_Id__c, 
                    Account.isPersonAccount,
                    Retailer__r.External_Id__c, 
                    Leasing_Company__r.External_Id__c, 
                    Retailer__c, 
                    Leasing_Company__c,
                    Tenant_Id__c
                From Asset 
                Where Id in :AssetRelations
        ];
    }

}