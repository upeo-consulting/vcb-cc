/**
 * @Version: 1.0
 * @Author: Frederik Pardon
 * @Copyright: 2021 4C Consulting/Wipro
 * @Uses:
 *
 * -----------------------------------------------------------------------------------------------
 * Description: Any logic that is needed for Record Ownership should be implemented in this class. The class has been made object generic.
 *
 *
 * Created: 07/04/2021
 * Last Updated: 07/04/2021
 *
 * Unit tests: 
 *
 * Change log:
 * -----------------------------------------------------------------------------------------------
 */


public with sharing class OwnerService {

    IUserSelector userSelector;

    public OwnerService(IUserSelector userSelector) {
        this.userSelector = userSelector;

    }

    public OwnerService() {
    }

// Method that updates the Owner based on the tenant
//
    public void setRecordOwner(map<Id, SObject> newMap, map<Id, SObject> oldMap) {


//Initiate lists and maps to be used
//


            List <SObject> listRecordsToProcess = new List<SObject>();
            List<String> ownersToQuery = new List<String>();
            List<SObject> listRecordsToUpdate = new List<SObject>();
            List<String> recordOwnerMatricesToQuery = new List<String>();
            List<String> criteriaToUse = new List<String>();
            List<SObject> updateList = new List<SObject>();

            map<String, Id> mapUser = new map<String, Id>();

            List<Record_Owner_Matrix_Rule__mdt> ownerMatrixRulesList = Record_Owner_Matrix_Rule__mdt.getAll().values();
            Map<String, Record_Owner_Matrix_Rule__mdt> ownerMatrixRuleMap = (Map<String, Record_Owner_Matrix_Rule__mdt>)Collections.mapByStringField(ownerMatrixRulesList, Record_Owner_Matrix_Rule__mdt.SObject__c);

            // add records from new map to listRecordsToProcess
            // Get the SObjectType we're working with and store it for later reference
            // Check whether the record's Tenant Id Field was updated
            
        if (oldMap == null) {
            system.debug('old map is null');
        }

            for(SObject record : newMap.values()) {
                
                if(oldMap != null) {
                   system.debug('oldMap size = ' + oldMap.values().size());
                   SObject sObj = oldMap.get(record.Id);
                   String oldTenant = (String) sObj.get('Tenant_Id__c');
                   String newTenant = (String) record.get('Tenant_Id__c');
                   if (oldTenant != newTenant) {
                       listRecordsToProcess.add(record);
                   }
                    

                } else {
                    listRecordsToProcess.add(record);
                }
            }
        
        if (listRecordsToProcess.size() > 0) {

            Schema.SObjectType sObjectType = listRecordsToProcess[0].getSObjectType();
            String typeName = String.valueOf(sObjectType);

            // Get Custom Metadata Type that contains the rules for the SObjectType we're working with

            Record_Owner_Matrix_Rule__mdt ownerMatrixRule = ownerMatrixRuleMap.get(typeName);


            // Split values from Criteria__c into list
            // Checks whether values were returned and process accordingly

            if (ownerMatrixRule.Criteria__c != null && ownerMatrixRule != null) {
                List<String> criteriaSeparatedList = ownerMatrixRule.Criteria__c.split(',');
                for (String separatedValue : criteriaSeparatedList) {
                    criteriaToUse.add(separatedValue);
                }
            }
            else if (ownerMatrixRule.Criteria__c == null && ownerMatrixRule != null) {
                criteriaToUse.add('Sobject');
            }


            // Retrieve Custom Metadata Records and cast them to a Map.

            List<Record_Owner_Matrix__mdt> getRecordOwnerMatrixRecords = Record_Owner_Matrix__mdt.getAll().values();
            Map<String, Record_Owner_Matrix__mdt> ownerMatrixMap = new Map<String, Record_Owner_Matrix__mdt>();
            
            for (Record_Owner_Matrix__mdt recordOwner : getRecordOwnerMatrixRecords) {
                String ownerMatrixMapKey = '';
                String sVarObjectType = recordOwner.SObject__c;
                String tenantValue = recordOwner.Tenant__c;
                String recordType ='';
                String type ='';
                if(recordOwner.RecordType__c != null) {
                    recordType = recordOwner.RecordType__c;
                }
                if(recordOwner.Type__c != null){
                    type = recordOwner.Type__c;
                }
                ownerMatrixMapKey = sVarObjectType  + tenantValue + recordType + type;
                ownerMatrixMap.put(ownerMatrixMapKey, recordOwner);
                system.debug('ownerMatrixMapKey = ' + ownerMatrixMapKey);
            }


            // Get owner type from Record Owner Matrix Map and use it to build the Query params used to get the correct User

            map<String, SObject> mapRecordOwnerKey = new map<string, SObject>();
            for (SObject record : listRecordsToProcess) {
                string recordKey = typeName +record.get('Tenant_Id__c');
                for (String criteria : criteriaToUse) {
                    system.debug(criteria);
                    if(criteria.trim() == 'RecordTypeId') {
                        String RTId = string.valueOf(record.get('RecordTypeId'));
                        String recordType = GlobalUtilsHelper.getRecordTypeDeveloperName(typeName, RTId);
                        recordKey = recordKey + recordType;
                    }
                    if(criteria.trim() == 'Type') {
                        recordKey = recordKey + record.get('Type');
                        system.debug('recordKey after Type ' +recordKey);
                    }
                    if(criteria.trim() == 'Type__c') {
                        recordKey = recordKey + record.get('Type__c');
                    }
                    }
                    system.debug('recordKey = ' +recordKey);
                    system.debug('record = ' +record);
                    mapRecordOwnerKey.put(recordKey, record);
    
                    Record_Owner_Matrix__mdt RecordOwnerMatrixRecord = ownerMatrixMap.get(recordKey);
                    system.debug('owner record = ' +recordOwnerMatrixRecord);
    
    
                    if(record.get('Tenant_Id__c') != null && RecordOwnerMatrixRecord != null) {
    
                        sObject sObj = Schema.getGlobalDescribe().get(typeName).newSObject();
                        User owner = new User(External_Id__c = RecordOwnerMatrixRecord.User_To_Share_With__c + RecordOwnerMatrixRecord.Tenant_To_Share_With__c);
                        system.debug('RecordOwnerMatrix = ' +RecordOwnerMatrixRecord);
                        sObj.putSobject('Owner', owner);
                        sObj.put('Id', record.Id);
                        updateList.add(sObj);
                        system.debug('Object for owner ' +sObj);
                    }
            }
            system.debug('updateList size = ' + updateList.size());
            update updateList;
        
        }
    }
}