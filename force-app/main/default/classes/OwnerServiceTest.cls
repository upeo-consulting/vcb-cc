@IsTest
public with sharing class OwnerServiceTest {
    
    @TestSetup
    static void createTestData(){

        //Get record types
        //
        
        Test.startTest();    

        Id accountRecordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Account', 'Standard_Account');
        Id standardAssetRecordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Asset', 'Standard_Asset');
        Id contactRecordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Contact', 'Standard_Contact');

        Profile profile = [SELECT Id FROM Profile WHERE Name = 'Marketing User'];

        //Create test records

        User retailerOwner1 = (User) TestFactory.createSObject(new User(FirstName = 'Test', LastName = 'Owner1',Tenant_Id__c = 'Test1', External_Id__c = 'Salesdhondt', UserName = 'test.owner1@volvo.be.test', email = 'test.owner@volvo.be.test', profileId = profile.id ), false);
        User retailOwner2 = (User) TestFactory.createSObject(new User(FirstName = 'Test', LastName = 'Owner', Tenant_Id__c = 'Test2', External_Id__c = 'Salesvermant', UserName = 'test.owner2@volvo.be.test', email = 'test.owner@volvo.be.test', profileId = profile.id ), false);


        List<User> userList = new List<User> {retailerOwner1, retailOwner2};
        insert userList;
        
        Account testAccount1 = (Account) TestFactory.createSObject(new Account( Name = 'TestAccount1', Tenant_Id__c = 'dhondt', RecordTypeId = accountRecordTypeId ), false);
        Account testAccount2 = (Account) TestFactory.createSObject(new Account( Name = 'TestAccount2', Tenant_Id__c = 'vermant', RecordTypeId = accountRecordTypeId ), false);
		Account testVCBAccount = (Account) TestFactory.createSObject(new Account( Name = 'VCB', External_Id__c = 'VCB', RecordTypeId = accountRecordTypeId ), false);
        
        List<Account> accountList = new List<Account> {testAccount1, testAccount2, testVCBAccount};
        insert accountList; 

        Contact testContact1 = (Contact) TestFactory.createSObject(new Contact(FirstName = 'Test', LastName = 'Contact 1', Tenant_Id__c = 'dhondt', AccountId = testAccount1.Id, RecordTypeId = contactRecordTypeId ), false);
        Contact testContact2 = (Contact) TestFactory.createSObject(new Contact(FirstName = 'Test',LastName = 'Contact 2', Tenant_Id__c = 'vermant', AccountId = testAccount2.Id, RecordTypeId = contactRecordTypeId ), false);
        
        List<Contact> contactList = new List<Contact>{testContact1, testContact2};
        insert contactList;

                                                                    
        Opportunity testOpportunity1 = (Opportunity) TestFactory.createSObject(new Opportunity(Name = 'Test1', Tenant_Id__c = 'dhondt', AccountId = testAccount1.Id,StageName = 'Open'), false);                                                                   
		Opportunity testOpportunity2 = (Opportunity) TestFactory.createSObject(new Opportunity(Name = 'Test2',Tenant_Id__c = 'vermant', AccountId = testAccount2.Id, StageName = 'Open'), false);      
        List<Opportunity> opportunityList = new List<Opportunity>{testOpportunity1, testOpportunity2};
        insert opportunityList;
        
        Test.stopTest();

        }

	@IsTest
    public static void testSetRecordOwner(){

    Set<String> accountNames = new Set<String> {'TestAccount1', 'TestAccount2'};
    Set<String> contactNames = new Set<String> {'Test Contact 1', 'Test Contact 2'};
    Set<String> opportunityNames = new Set<String> {'Test1', 'Test2'};
    Set<String> userNames = new set<String>{'test.owner1@volvo.be.test', 'test.owner2@volvo.be.test'};

    List<Account> testAccounts = [SELECT id, Name, OwnerId, Owner.Name FROM Account WHERE Name IN :accountNames];
    List<Contact> testContacts = [SELECT Id, Name, OwnerId, Owner.Name FROM Contact WHERE Name IN :contactNames];
    List<Opportunity> testOppties = [SELECT Id, Name, OwnerId, Owner.Name FROM Opportunity WHERE Name IN :opportunityNames];
    List<User> testUsers = [SELECT Id, UserName FROM User WHERE UserName IN :userNames];

    Map<String, Account> accountMap = (Map<String, Account>)Collections.mapByStringField(testAccounts, Account.Name);
    Map<String, Contact> contactMap = (Map<String, Contact>)Collections.mapByStringField(testContacts, Contact.Name);
    Map<String, Opportunity> opptyMap = (Map<String, Opportunity>)Collections.mapByStringField(testOppties, Opportunity.Name);
    Map<String, User> userMap = (Map<String, User>)Collections.mapByStringField(testUsers, User.UserName);

    Account account1 = accountMap.get('TestAccount1');
    Account account2 = accountMap.get('TestAccount2');
    Contact contact1 = contactMap.get('Test Contact 1');
    Contact contact2 = contactMap.get('Test Contact 2');
    Opportunity oppty1 = opptyMap.get('Test1');
    Opportunity oppty2 = opptyMap.get('Test2');
    User user1 = userMap.get('test.owner1@volvo.be.test');
    User user2 = userMap.get('test.owner2@volvo.be.test');

    system.debug(account1);
    system.debug(user1);

    //Test record owners
    System.assertEquals(account1.ownerId == user1.Id, false);
    System.assertEquals(account2.ownerId == user2.Id, false);
    System.assertEquals(contact1.ownerId == user1.Id, false);
    System.assertEquals(contact2.ownerId == user2.Id, false);
    System.assertEquals(oppty1.ownerId == user1.Id, false);
    System.assertEquals(oppty2.ownerId == user2.Id, false);

    }

}