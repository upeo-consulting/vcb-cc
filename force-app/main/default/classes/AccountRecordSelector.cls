/**
 * Created by Frederik on 3/08/2021.
 */

public with sharing class AccountRecordSelector implements IAccountRecordSelector {

    public static List<Asset> getRelatedAssets(List<Id> accIds) {

        return [SELECT Id
        FROM Asset
        WHERE AccountId in :accIds
        ];

    }

    public static List<Quote> getRelatedQuotes(List<Id> accIds) {

        return [SELECT Id
                FROM Quote
                WHERE AccountId in :accIds]
                ;

    }


}