/**
 * Created by Frederik on 8/07/2021.
 */

@IsTest
private class QuoteServiceTest {

    @TestSetup
    static void createTestData() {

        Id personRecType = GlobalUtils.getRecordTypeIdByObjectAndDeveloperName('Account', 'PersonAccount');
        Id standardRecType = GlobalUtils.getRecordTypeIdByObjectAndDeveloperName('Account', 'Standard_Account');
        Id contactRecType = GlobalUtils.getRecordTypeIdByObjectAndDeveloperName('Contact', 'Standard_Contact');

        List<Account> personAccountList = new List<Account>();
        List<Account> standardAccountList = new List<Account>();
        List<Contact> contactList = new List<Contact>();

        for (Integer i = 0; i < 10; i++) {
            Account person = new Account();
            person.LastName = 'TestPerson' +i;
            person.RecordTypeId = personRecType;
            personAccountList.add(person);
        }

        for (Integer i = 0; i < 10; i++) {
            Account standard = new Account();
            standard.Name = 'TestStandard' +i;
            standard.RecordTypeId = standardRecType;
            standard.Email__c = standard.name + '@test.com';
            standardAccountList.add(standard);
        }

        insert personAccountList;
        insert standardAccountList;

        for (Account account : standardAccountList) {
            Integer i = 0;
            Contact con = new Contact();
            con.LastName = 'TestStandard' +i;
            con.RecordTypeId = contactRecType;
            con.AccountId = account.id;
            con.Email_Work__c = con.name + '@test.com';
            i++;
            contactList.add(con);
        }

        insert contactList;

    }

    @IsTest
    static void setQuoteContactTest() {

        List<Account> personAccounts = [SELECT Id, PersonContactId FROM Account WHERE Name LIKE 'TestPerson'];
        List<Account> standardAccounts = [SELECT Id, PersonContactId, Email__c FROM Account WHERE Name LIKE 'TestStandard'];

        List<Opportunity> oppList = new List<Opportunity>();
        List<Quote> qteList = new List<Quote>();

        for (Account acc : personAccounts) {
            Opportunity opp = new Opportunity();
            opp.Name = acc.Name;
            opp.StageName = 'Open';
            opp.CloseDate = System.today();
            oppList.add(opp);
        }

        for (Account acc : standardAccounts) {
            Opportunity opp = new Opportunity();
            opp.Name = acc.Name;
            opp.StageName = 'Open';
            opp.CloseDate = System.today();
            oppList.add(opp);
        }



        for (Opportunity opp :oppList){
            Integer i = 0;
            Quote qte = new Quote();
            qte.name = opp.Name;
            qte.OpportunityId = opp.Id;
            qteList.add(qte);
            i++;
        }

        insert oppList;
        insert qteList;

        for (Quote personQuote : [SELECT Id, AccountId, ContactId FROM Quote WHERE Name LIKE '%TestPerson%']){
            System.assertEquals(personQuote.AccountId, personQuote.ContactId);
        }

        for (Quote standardQuote : [SELECT Id, AccountId, ContactId, Contact.AccountId FROM Quote WHERE Name LIKE '%TestStandard%']){
            System.assertNotEquals(standardQuote.AccountId, standardQuote.Contact.AccountId);
        }


    }

    @IsTest
    static void setSelektCarFieldsTest() {

        List<Account> standardAccounts = [SELECT Id, PersonContactId, Email__c FROM Account WHERE Name LIKE 'TestStandard'];

        List<Opportunity> oppList = new List<Opportunity>();
        List<Quote> qteList = new List<Quote>();
        List<Asset> asstList = new List<Asset>();


        for (Account acc : standardAccounts) {
            Opportunity opp = new Opportunity();
            opp.Name = acc.Name;
            opp.StageName = 'Open';
            opp.CloseDate = System.today();
            oppList.add(opp);
        }

        for (Opportunity opp :oppList){
            Integer i = 0;
            Quote qte = new Quote();
            qte.name = opp.Name;
            qte.OpportunityId = opp.Id;
            qte.VIN_SelektCar__c = 'VIN' +i;
            qteList.add(qte);
            i++;
        }

        for (Quote qte : qteList){
            Integer i = 0;
            Asset asst = new Asset();
            asst.Name = 'Test'+i;
            asst.Quote__c = qte.id;
            asst.Make__c = 'volvo';
            asstList.add(asst);
            i++;
        }

        insert oppList;
        insert qteList;
        insert asstList;

        for (Quote standardQuote : [SELECT Id, Make__c FROM Quote WHERE Name LIKE '%TestStandard%']){
            System.assertNotEquals('volvo', standardQuote.Make__c);
        }


    }
}