/**
 * Created by Frederik Pardon on 07/04/2021.
 */

public with sharing class AccountContactRelationTriggerHandler extends TriggerHandler {

        protected override void onAfterInsert(Map<Id, SObject> newMap) {
                IMCSyncSelector mCSyncSelector = (IMCSyncSelector) new MCSyncSelector();
                MarketingCloudSyncService marketingCloudSyncService = (MarketingCloudSyncService) new MarketingCloudSyncService(mCSyncSelector);

                if(RecursiveTriggerHandler.isFirstTime == true) {
                    	RecursiveTriggerHandler.isFirstTime = false;
                        marketingCloudSyncService.setCheckBoxes((map<Id, SObject>) newMap);
                }


        }

        protected override void onAfterUpdate(Map<Id, SObject> newMap, Map<Id, SObject> oldMap) {
                IMCSyncSelector mCSyncSelector = (IMCSyncSelector) new MCSyncSelector();
                MarketingCloudSyncService marketingCloudSyncService = (MarketingCloudSyncService) new MarketingCloudSyncService(mCSyncSelector);

                if(RecursiveTriggerHandler.isFirstTime == true) {
                    	RecursiveTriggerHandler.isFirstTime = false;
                        marketingCloudSyncService.setCheckBoxes((map<Id, SObject>) newMap);

                }
        }

}