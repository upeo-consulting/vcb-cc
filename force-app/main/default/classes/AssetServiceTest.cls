@IsTest
public with sharing class AssetServiceTest {

        @TestSetup
        static void createTestData(){
            
            RecursiveTriggerHandler.isFirstTime = false;
            
              //Get records Ids            
            Id accountRecordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Account', 'Standard_Account');
            Id standardAssetRecordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Asset', 'Standard_Asset');
            Id goldenAssetRecordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Asset', 'Asset_Golden_Record');
            Id contactRecordTypeId = globalUtils.getRecordTypeIdByObjectAndDeveloperName('Contact', 'Standard_Contact');
               
            //Create test records           
            Account customerAccountTest = (Account) TestFactory.createSObject(new Account(Name = 'Customer Account', RecordTypeId = accountRecordTypeId, External_Id__c = 'CustomerAccount'), false);
            Account customerAccountTest2 = (Account) TestFactory.createSObject(new Account(Name = 'Customer Account2', RecordTypeId = accountRecordTypeId, External_Id__c = 'CustomerAccount2'), false);
            Account retailerAccountTest = (Account) TestFactory.createSObject(new Account(Name = 'Retailer Account', RecordTypeId = accountRecordTypeId, External_Id__c = 'RetailerAccount'), false);
            Account leasingAccountTest = (Account) TestFactory.createSObject(new Account(Name = 'Leasing Account', RecordTypeId = accountRecordTypeId, External_Id__c = 'LeasingAccount'), false);
            Account parentAccountTest = (Account) TestFactory.createSObject(new Account(Name = 'VCB', recordTypeId = accountRecordTypeId, External_Id__c = 'VCB'), true);

            List<Account> listAccounts = new List<Account> {customerAccountTest, retailerAccountTest, leasingAccountTest,parentAccountTest};
            upsert listAccounts;
                        
            Contact customerContactTest = (Contact) TestFactory.createSObject(New Contact(FirstName = 'Customer', LastName= 'Contact', External_ID__c = 'CustomerContactTest', RecordTypeId = contactRecordTypeId, AccountId = customerAccountTest.Id), false);
			
            List<Contact> listContacts = new List<Contact>{customerContactTest};
            upsert listContacts;
            
            Asset standardAssetExistingVINTest = (Asset) TestFactory.createSObject(New Asset(Name = 'Standard Asset Existing VIN', RecordTypeId = standardAssetRecordTypeId, AccountId = customerAccountTest2.Id, ContactId = customerContactTest.Id, Leasing_Company__c = leasingAccountTest.Id, Retailer__c = retailerAccountTest.Id, VIN__c = '1111test'), false);
            Asset standardAssetNewVINTest = (Asset) TestFactory.createSObject(New Asset(Name = 'Standard Asset New VIN', RecordTypeId = standardAssetRecordTypeId, AccountId = customerAccountTest.Id, ContactId = customerContactTest.Id, Leasing_Company__c = leasingAccountTest.Id, Retailer__c = retailerAccountTest.Id, VIN__c = '2222test'), false);
            Asset goldenAssetTest = (Asset) TestFactory.createSObject(New Asset(Name = 'Golden Asset', RecordTypeId = goldenAssetRecordTypeId, AccountId = parentAccountTest.Id, VIN__c = '1111test', External_Id__c = '1111test_Golden'), false);        

            List<Asset> listAssets = new List<Asset>{standardAssetExistingVINTest, standardAssetNewVINTest, goldenAssetTest};
            upsert listAssets;
            
            List<Asset> createdAssets = [Select Id, Leasing_Company__c, Retailer__c, Name from Asset];
            
            
                  
    	}

    @IsTest
    public static void testaddAccountRelationsToAccount(){

        RecursiveTriggerHandler.isFirstTime = True;

        //Create sets with records to retrieve

        Test.startTest();

        List<Asset> assList = [SELECT Id, Type__c FROM Asset];

        For (Asset ass : assList ) {
            ass.Type__c = 'Test';
        }

        update assList;

        Test.stopTest();

        Set<String> accountNames = new Set<String> {'Customer Account', 'Retailer Account', 'Leasing Account'};
        Set<String> contactNames = new Set<String> {'Customer Contact'};

        List<Account> testAccounts = [SELECT id, Name FROM Account WHERE Name IN :accountNames];
        List<Contact> testContacts = [SELECT Id, Name From Contact WHERE Name IN :contactNames];

        Map<String, Account> accountMap = (Map<String, Account>)Collections.mapByStringField(testAccounts, Account.Name);
        Map<String, Contact> contactMap = (Map<String, Contact>)Collections.mapByStringField(testContacts, Account.Name);

        Account customerAccount = accountMap.get('Customer Account');
        Account retailerAccount = accountMap.get('Retailer Account');
        Account leasingAccount = accountMap.get('Leasing Account');
        Contact customerContact = contactMap.get('Customer Contact');

        List<Account_Relationship__c> allRelations = [SELECT id, Role__c, Account_From__c, Account_To__c FROM Account_Relationship__c];
        system.debug(allRelations.size());

        for (Account_Relationship__c acr : allRelations){
            system.debug('!!!! Role = ' + acr.Role__c + ' Account To = ' +acr.Account_To__c);
        }

        Account_Relationship__c retailerRelation = [SELECT id, Role__c, Account_From__c, Account_To__c FROM Account_Relationship__c WHERE Role__c = 'Retailer' LIMIT 1]; //WHERE Account_From__c = :customerAccount.Id AND Account_To__c = :retailerAccount.Id];
        Account_Relationship__c leasingRelation = [SELECT id, Role__c, Account_From__c, Account_To__c FROM Account_Relationship__c WHERE Account_From__c = :customerAccount.Id AND Account_To__c = :leasingAccount.Id];
        AccountContactRelation employerRelation = [SELECT id, Roles, Account.Id, Contact.Id FROM AccountContactRelation WHERE AccountId = :customerAccount.Id AND ContactId = :customerContact.Id];

        //Test Retailer Relation
        System.assertEquals(retailerRelation.Role__c == 'Retailer', true);
        System.assertEquals(retailerRelation.Role__c == 'Leasing Company', false);
        System.assertEquals(retailerRelation.Role__c == 'Golden Record', false);

        //Test Leasing Relation
        System.assertEquals(leasingRelation.Role__c == 'Retailer', false);
        System.assertEquals(leasingRelation.Role__c == 'Leasing Company', true);
        System.assertEquals(leasingRelation.Role__c == 'Golden Record', false);

        //Test Contact Relation
        System.assertEquals(employerRelation.Roles == 'Retailer', false);
        System.assertEquals(employerRelation.Roles == 'Leasing Company', false);
        System.assertEquals(employerRelation.Roles == 'Golden Record', false);
    }
    @IsTest
    public static void testaddGoldenAsset(){

            RecursiveTriggerHandler.isFirstTime = True;

            Set<String> accountNames = new Set<String> {'Customer Account', 'Retailer Account', 'Leasing Account'};
            Set<String> assetNames = new Set<String> {'Standard Asset Existing VIN', 'Standard Asset New VIN', 'Golden Asset', 'Standard Asset New VIN - Golden'};

            List<Account> testAccounts = [SELECT id, Name FROM Account WHERE Name IN :accountNames];
            List<Asset> testAssets = [SELECT Id,
                                      Name,
                                      RecordType.DeveloperName,
                                      External_Id__c,
                                      Ultimate_Parent__c,
                                      Ultimate_Parent__r.External_Id__c,
                                      VIN__c
                                      FROM Asset
                                      WHERE Name IN :assetNames];

            Map<String, Account> accountMap = (Map<String, Account>)Collections.mapByStringField(testAccounts, Account.Name);
            Map<String, Asset> assetMap = (Map<String, Asset>)Collections.mapByStringField(testAssets, Account.Name);

            Account customerAccount = accountMap.get('Customer Account');
            Account retailerAccount = accountMap.get('Retailer Account');
            Account leasingAccount = accountMap.get('Leasing Account');

            Asset goldenAsset = assetMap.get('Golden Asset');
            Asset standardAssetExistingVIN = assetMap.get('Standard Asset Existing VIN');
            Asset standardAssetNewVIN = assetMap.get('Standard Asset New VIN');
            Asset goldenAssetNewVIN = assetMap.get('Standard Asset New VIN - Golden');


        Test.startTest();
            List<Asset> listAsset = [SELECT Id, Type__c FROM Asset WHERE Name = 'Standard Asset New VIN' or Name = 'Standard Asset Existing VIN'];

            for(Asset ast : listAsset) {
            ast.Type__c = 'Test';
            }

            update listAsset;

        Test.stopTest();

        //Check record types
        System.assertEquals(goldenAsset.RecordType.DeveloperName == 'Asset_Golden_Record', true);
        System.assertEquals(standardAssetExistingVIN.RecordType.DeveloperName == 'Standard_Asset', true);
        System.assertEquals(standardAssetNewVIN.RecordType.DeveloperName == 'Standard_Asset', true);

        //Check parent records
        System.assertEquals(goldenAsset.Ultimate_Parent__c == null, true);
        System.assertEquals(standardAssetExistingVIN.Ultimate_Parent__c == goldenAsset.Id, true);
        System.assertEquals(standardAssetNewVIN.Ultimate_Parent__c == goldenAssetNewVIN.Id, true);

        //Check field values
        system.assertEquals(goldenAsset.VIN__c == standardAssetExistingVIN.VIN__c, true);
        system.assertEquals(goldenAsset.VIN__c == standardAssetNewVIN.VIN__c, false);
        system.assertEquals(goldenAssetNewVIN.VIN__c == standardAssetNewVIN.VIN__c, true);
        system.assertEquals(goldenAssetNewVIN.VIN__c == standardAssetExistingVIN.VIN__c, false);

    }

}