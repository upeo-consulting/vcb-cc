/**
 * Created by Frederik on 7/05/2021.
 */

public with sharing class ContactSelector implements IContactSelector{

    public static list<Contact> getContactByEmail(Map<Id, String> emails, String queryFields, List<String> accountIds) {

        return [
                SELECT Id
                FROM Contact
                Where AccountId in: accountIds
        ];

    }

}