/**
 * @description       : Abstract class to handle trigger events from any SObject
 * @author            : 4C Consulting NV
 * @group             : Trigger Handler
 * @last modified on  : 10-08-2020
 * @last modified by  : 4C Consulting NV
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   02-09-2019   Maarten.Devos@weare4c.com      Added extra methods to handle List<sObject> instead of Map<Id,sObject>
 * 1.1   25-06-2020   Glenn.Deschuymer@weare4c.com   Added a method to disable automations
**/
public abstract class TriggerHandler {
    @testVisible private Boolean isBefore;
    @testVisible private Boolean isAfter;
    @testVisible private Boolean isInsert;
    @testVisible private Boolean isUpdate;
    @testVisible private Boolean isDelete;
    @testVisible private Boolean isUndelete;
            
    @testVisible public TriggerEventType eventType { get; private set; }
    
    public TriggerHandler(){
        this.isBefore = Trigger.isBefore;
        this.isAfter = Trigger.isAfter;
        this.isInsert = Trigger.isInsert;
        this.isUpdate = Trigger.isUpdate;
        this.isDelete = Trigger.isDelete;
        this.isUndelete = Trigger.isUndelete;
    }

    /**
    * @description Determine if triggers have been disabled on company, profile or user level
    * @return Boolean 
    **/
	@testVisible
    private static Boolean triggersActivated(){
        if (Trigger_Activation__c.getOrgDefaults().Disable_All_Apex_Triggers__c){
            return false;
        } else if (Trigger_Activation__c.getInstance(UserInfo.getProfileId()).Disable_All_Apex_Triggers__c){
            return false;
        } else if (Trigger_Activation__c.getInstance(UserInfo.getUserId()).Disable_All_Apex_Triggers__c){
            return false;
        }        
        return true;
    }
    
    /**
    * @description Redirects the execution to appropriate method depending on the current values of Trigger state variables.
    * @return void 
    **/
    public virtual void run() {        
        if (!Test.isRunningTest() && !Trigger.isExecuting) return;
        if (!TriggerHandler.triggersActivated()) return;
        
        if (this.isBefore) {
            if (this.isInsert) {
                this.eventType = TriggerEventType.BEFORE_INSERT;
                this.onBeforeInsert((List<SObject>) Trigger.new);
            } else if (this.isUpdate) {
                this.eventType = TriggerEventType.BEFORE_UPDATE;
                this.onBeforeUpdate((Map<Id, SObject>) Trigger.newMap, (Map<Id, SObject>) Trigger.oldMap);
            } else if (this.isDelete) {
                this.eventType = TriggerEventType.BEFORE_DELETE;
                this.onBeforeDelete((Map<Id, SObject>) Trigger.oldMap);
            }
        } else if (this.isAfter) {
            if (this.isInsert) {
                this.eventType = TriggerEventType.AFTER_INSERT;
                this.onAfterInsert((Map<Id, SObject>) Trigger.newMap);
            } else if (this.isUpdate) {
                this.eventType = TriggerEventType.AFTER_UPDATE;
                this.onAfterUpdate((Map<Id, SObject>) Trigger.newMap, (Map<Id, SObject>) Trigger.oldMap);
            } else if (this.isDelete) {
                this.eventType = TriggerEventType.AFTER_DELETE;
                this.onAfterDelete((Map<Id, SObject>) Trigger.oldMap);
            } else if (this.isUndelete) {
                this.eventType = TriggerEventType.AFTER_UNDELETE;
                this.onAfterUndelete((Map<Id, SObject>) Trigger.newMap);
            }
        }
    }
  
    /**
     * Executed when "before insert" event occurs.
     *
     * In the "before insert" event the records being inserted don't have the Id values yet, so we cannot use newMap
     * from Trigger variable (which is more preferable approach in most cases as it gives the fast access to records
     * by their Ids when searching for individual record from the collection is needed).
     *
     * Implement the code to handle this event in the descendant classes. To get specific SObject type records,
     * use typecasting in the actual implementation.
     *
     * It has not been made abstract, instead of that we use empty body implementation. This is made this way
     * to avoid having to implement this method in descenants when it is not used.
     *
     * @param newList List<SObject> of records being inserted.
     */
    protected virtual void onBeforeInsert(List<SObject> newList) {}

    /**
     * Executed when "after insert" event occurs.
     *
     * In the "after insert" event the records being inserted already have the Id values, so now we can use newMap from
     * Trigger variables (which is more preferable approach in most cases as it gives the fast access to records by their
     * Ids when searching for individual record from the collection is needed). If the list is needed instead of a map -
     * we can get the list by calling newMap.values() method.
     *
     * Implement the code to handle this event in the descendant classes. To get specific SObject type records,
     * use typecasting in the actual implementation.
     *
     * It has not been made abstract, instead of that we use empty body implementation. This is made this way
     * to avoid having to implement this method in descenants when it is not used.
     *
     * @param newMap Map<Id, SObject> new version of the records being inserted.
     */
    protected virtual void onAfterInsert(Map<Id, SObject> newMap) {}

    /**
     * Executed when "before update" event occurs.
     * This is also the step number 3 in SObject merge operation (merge winning SObjects are updated).
     *
     * We can use oldMap and newMap from Trigger variables which is preferable approach in most cases as it gives
     * the fast access to records by their Ids  when searching for individual record from the collection is needed.
     * If the list is needed instead of a map - we can get the list by calling newMap.values() method.
     *
     * Implement the code to handle this event in the descendant classes. To get specific SObject type records,
     * use typecasting in the actual implementation.
     *
     * It has not been made abstract, instead of that we use empty body implementation. This is made this way
     * to avoid having to implement this method in descenants when it is not used.
     *
     * @param newMap Map<Id, SObject> new version of the records being updated.
     *
     * @param oldMap Map<Id, SObject> old version of the records being updated.
     */
    protected virtual void onBeforeUpdate(Map<Id, SObject> newMap, Map<Id, SObject> oldMap) {}

    /**
     * Executed when "after update" event occurs.
     * This is also the step number 4 in SObject merge operation (merge winning SObjects are updated).
     *
     * We can use oldMap and newMap from Trigger variables which is preferable approach in most cases as it gives
     * the fast access to records by their Ids  when searching for individual record from the collection is needed.
     * If the list is needed instead of a map - we can get the list by calling newMap.values() method.
     *
     * Implement the code to handle this event in the descendant classes. To get specific SObject type records,
     * use typecasting in the actual implementation.
     *
     * It has not been made abstract, instead of that we use empty body implementation. This is made this way
     * to avoid having to implement this method in descenants when it is not used.
     *
     * @param newMap Map<Id, SObject> new version of the records being updated.
     *
     * @param oldMap Map<Id, SObject> old version of the records being updated.
     */
    protected virtual void onAfterUpdate(Map<Id, SObject> newMap, Map<Id, SObject> oldMap) {}

    /**
     * Executed when "before delete" event occurs.
     * This is also the step number 1 in SObject merge operation (merge losing SObjects are deleted).
     *
     * We can use oldMap from Trigger variables which is preferable approach in most cases as it gives the fast
     * access to records by their Ids  when searching for individual record from the collection is needed. If the
     * list is needed instead of a map - we can get the list by calling newMap.values() method.
     *
     * Implement the code to handle this event in the descendant classes. To get specific SObject type records,
     * use typecasting in the actual implementation.
     *
     * It has not been made abstract, instead of that we use empty body implementation. This is made this way
     * to avoid having to implement this method in descenants when it is not used.
     *
     * @param oldMap Map<Id, SObject> old version of the records being deleted.
     */
    protected virtual void onBeforeDelete(Map<Id, SObject> oldMap) {}

    /**
     * Executed when "after delete" event occurs.
     * This is also the step number 2 in SObject merge operation (merge losing SObjects are deleted).
     *
     * We can use oldMap from Trigger variables which is preferable approach in most cases as it gives the fast
     * access to records by their Ids  when searching for individual record from the collection is needed. If the
     * list is needed instead of a map - we can get the list by calling newMap.values() method.
     *
     * Implement the code to handle this event in the descendant classes. To get specific SObject type records,
     * use typecasting in the actual implementation.
     *
     * It has not been made abstract, instead of that we use empty body implementation. This is made this way
     * to avoid having to implement this method in descenants when it is not used.
     *
     * @param oldMap Map<Id, SObject> old version of the records being deleted.
     */
    protected virtual void onAfterDelete(Map<Id, SObject> oldMap) {}

    /**
     * Executed when "after undelete" event occurs.
     *
     * We can use newMap from Trigger variables which is preferable approach in most cases as it gives the fast
     * access to records by their Ids  when searching for individual record from the collection is needed. If the
     * list is needed instead of a map - we can get the list by calling newMap.values() method.
     *
     * Implement the code to handle this event in the descendant classes. To get specific SObject type records,
     * use typecasting in the actual implementation.
     *
     * It has not been made abstract, instead of that we use empty body implementation. This is made this way
     * to avoid having to implement this method in descenants when it is not used.
     *
     * @param newMap Map<Id, SObject> map of the records being undeleted.
     */
    protected virtual void onAfterUndelete(Map<Id, SObject> newMap) {}
}