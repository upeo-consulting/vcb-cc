/**
 * Created by Frederik Pardon on 07/04/2021.
 */

public with sharing class UserSelector implements IUserSelector {
    public static List<User> getOwnerByTenant(List<String> tenants) {
        return [
                SELECT Id, External_Id__c, Tenant_Id__c
                FROM User
                WHERE External_Id__c IN: tenants
        ];
    }   
    
}