@IsTest public with sharing class AccountContactMarketingCloudSynTest{

   // If the CMD record has 'null' for the Email_Field__c we do not need to compare values and can simply mark the checkbox as TRUE
    @IsTest static void accountObjectCMDNullEmailFieldTest() {
   
          Id standardAccount =Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Standard_Account').getRecordTypeId();
          Account account = new Account();
          account.RecordtypeId=standardAccount;
          account.Name='StandardAccount';
          Insert account; 
          List<Account> accountRecord= [SELECT Id, Sync_to_MC__c FROM Account where id=:account.Id];
          System.assertEquals(True,accountRecord[0].Sync_to_MC__c);
     }
    // If the CMD record is not null and the values to compare on the record is not null we update the checkbox to TRUE.    
     @IsTest static void accountObjectCMDNotNullTest() {
     
          Id personAccount =Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
          Account account = new Account();
          account.RecordtypeId=personAccount;
          account.LastName='Test Account';
          account.Email_Work__pc='Testwork@gmail.com'; 
          account.PersonEmail='TestPE@gmail.com';  
          Insert account; 
          List<Account> accountRecord= [SELECT Id, Sync_to_MC__c FROM Account where id=:account.Id];
          System.assertEquals(True,accountRecord[0].Sync_to_MC__c);
     } 
     // If the CMD record is not null and the values to compare on the record is not null we update the checkbox to TRUE.    
     @IsTest static void accountObjectCMDNotNullTestUpdate() {
     
          Id personAccount =Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
          Account account = new Account();
          account.RecordtypeId=personAccount;
          account.LastName='Test Account';
          Insert account;
          account.Email_Work__pc='Testwork@gmail.com'; 
          account.PersonEmail='TestPE@gmail.com';
          RecursiveTriggerHandler.isFirstTime = true ; 
          Update account; 
          List<Account> accountRecord= [SELECT Id, Sync_to_MC__c FROM Account where id=:account.Id];
          System.assertEquals(True,accountRecord[0].Sync_to_MC__c);
     } 
     // If the CMD record is not null and the values to compare on the record is not null we update the checkbox to FALSE.
     @IsTest static void accountObjectCMDNullTest() {
     
          Id personAccount =Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
          Account account = new Account();
          account.RecordtypeId=personAccount;
          account.LastName='Test Account';
          Insert account; 
          List<Account> accountRecord= [SELECT Id, Sync_to_MC__c FROM Account where id=:account.Id];
          System.assertEquals(False,accountRecord[0].Sync_to_MC__c);
     }
    // If the CMD record is not null and the values to compare on the record is not null we update the checkbox to TRUE.    
    @IsTest static void contactObjectCMDNotNullEmailFieldTest() {
   
          Id standardContact =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Standard_Contact').getRecordTypeId();
          Contact contact = new Contact();
          contact.RecordtypeId=standardContact;
          contact.LastName='StandardContact';
          contact.Email='stdcon@gmail.com';
          contact.Email_Work__c='stdconwork@gmail.com';
          Insert contact; 
          List<Contact> contactRecord= [SELECT Id, Sync_to_MC__c FROM Contact where id=:contact.Id];
          System.assertEquals(TRUE,contactRecord[0].Sync_to_MC__c);
     }
     // If the CMD record is not null and the values to compare on the record is not null we update the checkbox to TRUE.    
    @IsTest static void contactObjectCMDNotNullEmailFieldTestUpdate() {
   
          Id standardContact =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Standard_Contact').getRecordTypeId();
          Contact contact = new Contact();
          contact.RecordtypeId=standardContact;
          contact.LastName='StandardContact';
          Insert contact;
          contact.Email='stdcon@gmail.com';
          contact.Email_Work__c='stdconwork@gmail.com';
          RecursiveTriggerHandler.isFirstTime = true ;
          Update contact; 
          List<Contact> contactRecord= [SELECT Id, Sync_to_MC__c FROM Contact where id=:contact.Id];
          System.assertEquals(TRUE,contactRecord[0].Sync_to_MC__c);
     }
    //Contact Object CMD NullEmail Field Negative Test Cases.  
     @IsTest static void contactObjectCMDNullEmailFieldTest() {
   
          Id standardContact =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Standard_Contact').getRecordTypeId();
          Contact contact = new Contact();
          contact.RecordtypeId=standardContact;
          contact.LastName='StandardContact';
          Insert contact; 
          List<Contact> contactRecord= [SELECT Id, Sync_to_MC__c FROM Contact where id=:contact.Id];
          System.assertEquals(False,contactRecord[0].Sync_to_MC__c);
     }
 
}