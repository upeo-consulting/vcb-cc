/**
 * Created by Frederik on 30/06/2021.
 */

public with sharing class DuplicateSelector implements IDuplicateSelector {

    public static List<Sobject> getDuplicateRecords(list<String> externalIds, list<String> Ids, String objectType) {

        string selectString = 'SELECT Id, External_Id__c ';
        string fromString = ' FROM ' +objectType;
        string whereString = ' WHERE External_Id__c in :externalIds AND Id NOT in :Ids';

        return database.query(selectString + fromString + whereString);

    }

    public static List<SObject> getRecordsToDelete(list<String> deleteIds, String objectType) {

        string selectString = 'SELECT Id';
        string fromString = ' FROM ' +objectType;
        string whereString = ' WHERE Id in :deleteIds';

        return database.query(selectString + fromString + whereString);


    }

}