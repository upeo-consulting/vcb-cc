/**
 * @Version: 1.0
 * @Author: Frederik Pardon
 * @Copyright: 2021 4C Consulting/Wipro
 * @Uses:
 *
 * -----------------------------------------------------------------------------------------------
 * Description: Class that drives the ticking of the checkboxes used to indicate whether a record should be synced to Marketing Cloud.
 * The class has been made object generic. The class depends on MarketingCloudSyncBoxRelations__mdt in which we store per SObject type which
 * fields and query statements are used for the query in MCSyncSelector. The MCSyncSelector returns the list of records to be updated.
 *
 *
 * Created: 07/04/2021
 * Last Updated: 07/04/2021
 *
 * Dependencies: MarketingCloudSyncBoxRelations__mdt
 *
 * Unit tests:
 *
 * Change log:
 * -----------------------------------------------------------------------------------------------
 */

public with sharing class MarketingCloudSyncService {

    IMCSyncSelector mCSyncSelector;

    public MarketingCloudSyncService(IMCSyncSelector mCSyncSelector) {
        this.mCSyncSelector = mCSyncSelector;
    }

    public MarketingCloudSyncService() {

    }

    public void setCheckBoxes(map<Id, SObject> newMap) {
        system.debug('Class was called');

        //  Create lists and maps to be used throughout the class.
        List<SObject> listRecordsToProcess = new List<SObject>();
        List<String> accountsToQuery = new List<String>();
        List<SObject> recordsToUpdate = new List<SObject>();

        // Create a map to store all Custom Metadata records
        Map<String, MarketingCloudSyncBoxRelations__mdt> mapRelationFields = new Map<String, MarketingCloudSyncBoxRelations__mdt>();
        List<MarketingCloudSyncBoxRelations__mdt> relationFields = MarketingCloudSyncBoxRelations__mdt.getAll().values();

        for(SObject record : newMap.values()){
            listRecordsToProcess.add(record);
        }

        // get the SObjectType we're working with
        Schema.SObjectType sObjectType = listRecordsToProcess[0].getSObjectType();
        String objectType= String.valueOf(sObjectType);
        system.debug('objectType = ' +objectType);

        // Add the SObject type and the CMD record to mapRelationFields
        for(MarketingCloudSyncBoxRelations__mdt relations : relationFields) {
            mapRelationFields.put(relations.SObject__c, relations);
        }

        system.debug(mapRelationFields);

        // Get the matching record from mapRelationFields based on the SObject type we're working with
        MarketingCloudSyncBoxRelations__mdt recordMCMetaData = mapRelationFields.get(objectType);
        // Process records only when a matching CMD type was found in mapRelationFields. If a matching record was found in the map,
        // we use Relationship_Fields__c, Field_To_Update__c and the Id of the record being processed in the call to MCSyncSelector/
        if (recordMCMetaData != null) {
            String relationFieldValues = (String) recordMCMetaData.Relationship_Fields__c;
            String fieldToUpdate = (String) recordMCMetaData.Field_To_Update__c;

            for (SObject record : newMap.values()) {
                accountsToQuery.add(record.Id);
            }

            // Call MCSyncSelector with the parameters we retrieved from mapRelationFields, the SObject and the Field to update
            list<SObject> queryList = mCSyncSelector.getRecordsById(accountsToQuery, objectType, relationFieldValues, fieldToUpdate);
            system.debug(queryList);

            // Process the returned list of records and add them to a list to be used for the update statement
            system.debug('queryList size = ' + querylist.size());
            if (queryList.size() > 0) {
                for (SObject record : queryList) {
                    SObject recordToUpdate = Schema.getGlobalDescribe().get(objectType).newSObject() ;
                    recordToUpdate.put(fieldToUpdate, TRUE);
                    recordToUpdate.put('Id', record.Id);
                    system.debug('record values are : ' + recordToUpdate);
                    recordsToUpdate.add(recordToUpdate);
                }
            }

        }

        // update the returned and processed records
            update recordsToUpdate;

        }
}