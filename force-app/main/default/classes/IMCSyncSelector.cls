/**
 * Created by Frederik on 20/04/2021.
 */

public interface IMCSyncSelector {
    List<Sobject> getRecordsById(list<String> Ids, String objectType, String fieldNames, String fieldToUpdate);
}