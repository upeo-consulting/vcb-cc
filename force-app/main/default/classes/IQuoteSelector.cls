public interface IQuoteSelector {
    
    List<Quote> getAccountRecordTypeByQuoteId (List<String> quoteIds);
    List<AccountContactRelation> getContactsByEmail (List<Quote> quotes);
    list<Asset> getQuoteAssets(List<String> quoteIds);
}